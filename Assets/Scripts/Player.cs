﻿using System.Collections;
using UnityEngine;
using DG.Tweening;  // 追加

namespace Chankou {

    /* 味方アニマルのクラス */

    public class Player : Animal {

        readonly Vector3 SIZE1 = new Vector3 (1f, 1f, 1f);
        readonly Vector3 SIZE2 = new Vector3 (1.2f, 1.2f, 1f);

        Vector3 screenPoint;
        Vector3 offset;
        int xPosIndex;  // アニマルの列番号
        Coroutine coroutine;
        bool onMouse;

        protected override void Awake () {
            base.Awake ();
        }

        void Start () {
            gameObject.SetActive (false);
        }

        // 現れる
        public void Active (int ID, int index) {
            AnimalID = ID;
            myTrans.position = PositionManager.Instance.PlayerPos[index];
            xPosIndex = index;

            // サイズ、オーダー、角度を元に戻す
            myTrans.localScale = SIZE1;
            spriteRenderer.sortingOrder = 0;
            myTrans.rotation = Quaternion.Euler (0f, 0f, 0f);

            gameObject.SetActive (true);
        }

        // 移動する
        public void Move (int index) {
            xPosIndex = index;
            base.Move (PositionManager.Instance.PlayerPos[index]);
        }

        // 攻撃する動作
        public void AttackAction (int index) {
            if (index < Constant.LOWEST_ROW_INDEX) {  //  = if (index < 48)
                myTrans.DOMove (PositionManager.Instance.EnemyPos[index + Constant.ANIMAL_NUM], 0.4f).SetEase (Ease.OutBack);
            } else {
                Sequence sequence = DOTween.Sequence ();
                sequence.Append (myTrans.DOMoveY (PositionManager.Instance.EnemyPos[index].y - 0.32f, 0.1f));
            }
        }

        // 攻撃後に戻る動作
        public void Back () {
            // 0.2秒で戻る
            myTrans.DOMove (PositionManager.Instance.PlayerPos[xPosIndex], 0.2f);
        }

        // 掴まれている時の動作（震える）
        IEnumerator TakeAction () {
            float waitTime = 0.05f;
            float angle = 5f;  // 震える角度
            while (true) {
                yield return new WaitForSeconds (waitTime);
                myTrans.rotation = Quaternion.Euler (0f, 0f, angle);
                yield return new WaitForSeconds (waitTime);
                myTrans.rotation = Quaternion.Euler (0f, 0f, -angle);
            }
        }


        // 味方アニマルをタッチした時
        void OnMouseDown () {
            if (GameManager.Instance.isAction) {
                return;
            }
            onMouse = true;
            myTrans.localScale = SIZE2;  // 大きくする
            spriteRenderer.sortingOrder = 1;  // 前に出す
            coroutine = StartCoroutine (TakeAction ());

            // マウスカーソルは、スクリーン座標なので、
            // 対象のオブジェクトもスクリーン座標に変換してから計算する。

            // このオブジェクトの位置(transform.position)をスクリーン座標に変換。
            screenPoint = Camera.main.WorldToScreenPoint (myTrans.position);
            // ワールド座標上の、マウスカーソルと、対象の位置の差分。
            offset = myTrans.position - Camera.main.ScreenToWorldPoint (new Vector3 (Input.mousePosition.x, Constant.PLAYER_POS_Y, screenPoint.z));
        }

        // 味方アニマルをドラッグしている時
        void OnMouseDrag () {
            if (onMouse == false) {
                return;
            }
            Vector3 currentScreenPoint = new Vector3 (Input.mousePosition.x, Constant.PLAYER_POS_Y, screenPoint.z);
            Vector3 currentPosition = Camera.main.ScreenToWorldPoint (currentScreenPoint) + this.offset;
            myTrans.position = currentPosition;
            int after = PositionManager.Instance.GetPlayerPosIndex (currentPosition);
            int defference = after - xPosIndex;
            if (defference != 0) {
                switch (defference) {
                    case 2:
                        PlayerManager.Instance.ChangePos (xPosIndex, after - 1);
                        PlayerManager.Instance.ChangePos (xPosIndex + 1, after);
                        break;
                    case 3:
                        PlayerManager.Instance.ChangePos (xPosIndex, after - 2);
                        PlayerManager.Instance.ChangePos (xPosIndex + 1, after - 1);
                        PlayerManager.Instance.ChangePos (xPosIndex + 2, after);
                        break;
                    case 4:
                        PlayerManager.Instance.ChangePos (xPosIndex, after - 3);
                        PlayerManager.Instance.ChangePos (xPosIndex + 1, after - 2);
                        PlayerManager.Instance.ChangePos (xPosIndex + 2, after - 1);
                        PlayerManager.Instance.ChangePos (xPosIndex + 3, after);
                        break;
                    case 5:
                        PlayerManager.Instance.ChangePos (xPosIndex, after - 4);
                        PlayerManager.Instance.ChangePos (xPosIndex + 1, after - 3);
                        PlayerManager.Instance.ChangePos (xPosIndex + 2, after - 2);
                        PlayerManager.Instance.ChangePos (xPosIndex + 3, after - 1);
                        PlayerManager.Instance.ChangePos (xPosIndex + 4, after);
                        break;
                    case -2:
                        PlayerManager.Instance.ChangePos (xPosIndex, after + 1);
                        PlayerManager.Instance.ChangePos (xPosIndex - 1, after);
                        break;
                    case -3:
                        PlayerManager.Instance.ChangePos (xPosIndex, after + 2);
                        PlayerManager.Instance.ChangePos (xPosIndex - 1, after + 1);
                        PlayerManager.Instance.ChangePos (xPosIndex - 2, after);
                        break;
                    case -4:
                        PlayerManager.Instance.ChangePos (xPosIndex, after + 3);
                        PlayerManager.Instance.ChangePos (xPosIndex - 1, after + 2);
                        PlayerManager.Instance.ChangePos (xPosIndex - 2, after + 1);
                        PlayerManager.Instance.ChangePos (xPosIndex - 3, after);
                        break;
                    case -5:
                        PlayerManager.Instance.ChangePos (xPosIndex, after + 4);
                        PlayerManager.Instance.ChangePos (xPosIndex - 1, after + 3);
                        PlayerManager.Instance.ChangePos (xPosIndex - 2, after + 2);
                        PlayerManager.Instance.ChangePos (xPosIndex - 3, after + 1);
                        PlayerManager.Instance.ChangePos (xPosIndex - 4, after);
                        break;
                    default:
                        PlayerManager.Instance.ChangePos (xPosIndex, after);
                        break;
                }
                xPosIndex = after;
            }
        }

        // 味方アニマルから指を離した時
        void OnMouseUp () {
            if (onMouse == false) {
                return;
            }
            onMouse = false;
            StopCoroutine (coroutine);

            // サイズ、オーダー、角度を元に戻す
            myTrans.localScale = SIZE1;
            spriteRenderer.sortingOrder = 0;
            myTrans.rotation = Quaternion.Euler (0f, 0f, 0f);

            // 位置を正確にする
            myTrans.position = PositionManager.Instance.PlayerPos[xPosIndex];

            // プレイヤーが移動したならば、ターンを経過させる
            if (PlayerManager.Instance.isMove) {
                PlayerManager.Instance.isMove = false;
                GameManager.Instance.EveryTurn ();
            }
        }
    }
}
