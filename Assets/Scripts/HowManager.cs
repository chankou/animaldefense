﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;  // 追加
using DG.Tweening;  // 追加

namespace Chankou {

    /* 遊び方説明時のクラス */

    public class HowManager : SingletonMonoBehaviour<HowManager> {

        readonly Vector3[] vector = {
            new Vector3 (-180f, -350f, 0f),
            new Vector3 (-60f, -350f, 0f),
            new Vector3 (60f, -350f, 0f),
            new Vector3 (180f, -350f, 0f)
        };
        readonly Vector3 SIZE1 = new Vector3 (1f, 1f, 1f);
        readonly Vector3 SIZE2 = new Vector3 (1.2f, 1.2f, 1f);

        Coroutine coroutine;
        [SerializeField] AdMobManager admobManager;  // AdMob
        [SerializeField] Text text_how;
        [SerializeField] Text text_enemy;
        [SerializeField] Text text_friend;
        [SerializeField] Text text_skill;
        [SerializeField] Transform[] playerTrans;
        [SerializeField] EnemyForHowToPlay[] enemyArray1;
        [SerializeField] EnemyForHowToPlay[] enemyArray2;
        [SerializeField] GameObject arrowImage;
        [SerializeField] GameObject skillButton;
        [SerializeField] GameObject backButton;

        // ページのプロパティ
        int page;
        public int Page {
            private set {
                page = value;
                if (page == 1) {
                    backButton.SetActive (false);
                } else {
                    backButton.SetActive (true);
                }
            }
            get { return page; }
        }

        void Start () {
            // ローカライズ
            text_enemy.text = TextManager.Get (TextManager.KEY.ENEMY);
            text_friend.text = TextManager.Get (TextManager.KEY.FRIEND);
            text_skill.text = TextManager.Get (TextManager.KEY.SKILL);
            gameObject.SetActive (false);
        }

        // 初期化
        public void Init () {
            // アニメをストップする
            StopAnimalAnime ();
            playerTrans[0].localScale = SIZE1;
            // 味方アニマルの位置を初期化する
            for (int i = 0; i < playerTrans.Length; i++) {
                playerTrans[i].localPosition = vector[i];
            }
            // 敵アニマルを初期化する
            for (int i = 0; i < enemyArray1.Length; i++) {
                enemyArray1[i].Init ();
            }
            // 敵アニマルを初期化する
            for (int i = 0; i < enemyArray2.Length; i++) {
                enemyArray2[i].Init ();
            }
            arrowImage.SetActive (false);
            skillButton.SetActive (false);
        }

        // 遊び方を見せる
        public void Show () {
            admobManager.HideBanner ();
            Page = 1;
            gameObject.SetActive (true);
            Init ();
            GameManager.Instance.isAction = true;
            coroutine = StartCoroutine (HowAnimation1 ());
        }

        // 次のページへ行くボタン
        public void NextButton () {
            Page++;
            Init ();
            StartCoroutine (ChangeCoroutine ());
        }

        // 前のページに戻るボタン
        public void BackButton () {
            Page--;
            Init ();
            StartCoroutine (ChangeCoroutine ());
        }

        // 1ページ目
        IEnumerator HowAnimation1 () {
            text_how.text = TextManager.Get (TextManager.KEY.HOW1);

            // 0.6秒待つ
            yield return new WaitForSeconds (0.6f);

            Init ();
            arrowImage.SetActive (true);
        }

        // 2ページ目
        IEnumerator HowAnimation2 () {
            text_how.text = TextManager.Get (TextManager.KEY.HOW2);
            yield return new WaitForSeconds (0.6f);
            while (true) {
                Init ();

                yield return new WaitForSeconds (1f);

                playerTrans[0].localScale = SIZE2;

                yield return new WaitForSeconds (0.5f);

                playerTrans[0].DOLocalMoveX (180f, 0.6f).SetEase (Ease.Linear);
                playerTrans[1].DOLocalMoveX (-180f, 0.2f).SetEase (Ease.Linear);

                yield return new WaitForSeconds (0.2f);

                playerTrans[2].DOLocalMoveX (-60f, 0.2f).SetEase (Ease.Linear);

                yield return new WaitForSeconds (0.2f);

                playerTrans[3].DOLocalMoveX (60f, 0.2f).SetEase (Ease.Linear);

                yield return new WaitForSeconds (0.5f);

                playerTrans[0].localScale = SIZE1;

                yield return new WaitForSeconds (2f);
            }
        }

        // 3ページ目
        IEnumerator HowAnimation3 () {
            text_how.text = TextManager.Get (TextManager.KEY.HOW3);

            yield return new WaitForSeconds (0.6f);

            while (true) {
                Init ();
                yield return new WaitForSeconds (1f);

                playerTrans[0].localScale = SIZE2;

                yield return new WaitForSeconds (0.5f);

                playerTrans[0].DOLocalMoveX (180f, 0.6f).SetEase (Ease.Linear);
                playerTrans[1].DOLocalMoveX (-180f, 0.2f).SetEase (Ease.Linear);

                yield return new WaitForSeconds (0.2f);

                playerTrans[2].DOLocalMoveX (-60f, 0.2f).SetEase (Ease.Linear);

                yield return new WaitForSeconds (0.2f);

                playerTrans[3].DOLocalMoveX (60f, 0.2f).SetEase (Ease.Linear);

                yield return new WaitForSeconds (0.5f);

                playerTrans[0].localScale = SIZE1;

                yield return new WaitForSeconds (1f);

                playerTrans[1].DOLocalMoveY (220f, 0.4f).SetEase (Ease.OutBack);

                enemyArray1[0].DeActive ();

                yield return new WaitForSeconds (0.5f);

                playerTrans[3].DOLocalMoveY (220f, 0.4f).SetEase (Ease.OutBack);

                enemyArray1[1].DeActive ();

                yield return new WaitForSeconds (0.5f);

                playerTrans[0].DOLocalMoveY (220f, 0.4f).SetEase (Ease.OutBack);

                enemyArray1[2].DeActive ();

                yield return new WaitForSeconds (0.5f);

                playerTrans[1].DOLocalMoveY (-350f, 0.2f);
                playerTrans[3].DOLocalMoveY (-350f, 0.2f);
                playerTrans[0].DOLocalMoveY (-350f, 0.2f);

                yield return new WaitForSeconds (2f);
            }
        }

        // 4ページ目
        IEnumerator HowAnimation4 () {
            text_how.text = TextManager.Get (TextManager.KEY.HOW4);

            yield return new WaitForSeconds (0.6f);

            while (true) {
                Init ();
                playerTrans[0].localPosition = vector[3];
                playerTrans[1].localPosition = vector[0];
                playerTrans[2].localPosition = vector[1];
                playerTrans[3].localPosition = vector[2];
                enemyArray1[0].gameObject.SetActive (false);
                enemyArray1[1].gameObject.SetActive (false);
                enemyArray1[2].gameObject.SetActive (false);

                yield return new WaitForSeconds (1f);

                playerTrans[0].localScale = SIZE2;

                yield return new WaitForSeconds (0.5f);

                playerTrans[0].DOLocalMoveX (-60f, 0.4f).SetEase (Ease.Linear);
                playerTrans[3].DOLocalMoveX (180f, 0.2f).SetEase (Ease.Linear);

                yield return new WaitForSeconds (0.2f);

                playerTrans[2].DOLocalMoveX (60f, 0.2f).SetEase (Ease.Linear);

                yield return new WaitForSeconds (0.5f);

                playerTrans[0].localScale = SIZE1;

                yield return new WaitForSeconds (0.5f);

                playerTrans[0].DOLocalMoveY (220f, 0.4f).SetEase (Ease.OutBack);
                for (int i = 0; i < enemyArray2.Length; i++) {
                    enemyArray2[i].DeActive ();  // 敵を消す
                }

                yield return new WaitForSeconds (0.5f);

                playerTrans[0].DOLocalMoveY (-350f, 0.2f);

                yield return new WaitForSeconds (2f);
            }
        }

        // 5ページ目
        IEnumerator HowAnimation5 () {
            text_how.text = TextManager.Get (TextManager.KEY.HOW5);

            yield return new WaitForSeconds (0.6f);

            Init ();
            skillButton.SetActive (true);
        }

        // 6ページ目
        IEnumerator HowAnimation6 () {
            text_how.text = TextManager.Get (TextManager.KEY.HOW6);

            yield return new WaitForSeconds (0.6f);

            Init ();
        }

        // コルーチンの切り替わり
        IEnumerator ChangeCoroutine () {
            if (coroutine != null) {
                StopCoroutine (coroutine);
            }
            yield return 0;
            switch (Page) {
                case 1:
                    coroutine = StartCoroutine (HowAnimation1 ());
                    break;
                case 2:
                    coroutine = StartCoroutine (HowAnimation2 ());
                    break;
                case 3:
                    coroutine = StartCoroutine (HowAnimation3 ());
                    break;
                case 4:
                    coroutine = StartCoroutine (HowAnimation4 ());
                    break;
                case 5:
                    coroutine = StartCoroutine (HowAnimation5 ());
                    break;
                case 6:
                    coroutine = StartCoroutine (HowAnimation6 ());
                    break;
                case 7:
                    GameManager.Instance.isAction = false;  // 操作を有効
                    gameObject.SetActive (false);
                    break;
            }
        }

        // アニマルにアニメをやめさせる
        public void StopAnimalAnime () {
            for (int i = 0; i < enemyArray1.Length; i++) {
                enemyArray1[i].StopAnime ();
            }
            for (int i = 0; i < enemyArray2.Length; i++) {
                enemyArray2[i].StopAnime ();
            }
        }
    }
}
