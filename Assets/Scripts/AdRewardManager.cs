﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GoogleMobileAds.Api;  // 追加
using System;  // 追加

namespace Chankou {

    /* Google AdMobの動画リワード広告のクラス */

    public class AdRewardManager : SingletonMonoBehaviour<AdRewardManager> {

        void Start () {
            RegistHandler ();
            RequestRewardBasedVideo ();
        }

        public void RequestRewardBasedVideo () {
#if UNITY_ANDROID
            string adUnitId = "ca-app-pub-5022505207706955/4973437621";
#elif UNITY_IOS
            string adUnitId = "ca-app-pub-5022505207706955/2019971220";
#else
            string adUnitId = "unexpected_platform";
#endif

            RewardBasedVideoAd rewardBasedVideo = RewardBasedVideoAd.Instance;

            AdRequest request = new AdRequest.Builder ().Build ();
            // テストdevice
            //      AdRequest request = new AdRequest.Builder().AddTestDevice ("8d6cfcb87eef14c39585441b492adb8f").Build ();
            rewardBasedVideo.LoadAd (request, adUnitId);
        }

        void RegistHandler () {
            RewardBasedVideoAd rewardBasedVideo = RewardBasedVideoAd.Instance;
            // Ad event fired when the rewarded video ad
            // has been received.
            rewardBasedVideo.OnAdLoaded += HandleRewardBasedVideoLoaded;
            // has failed to load.
            rewardBasedVideo.OnAdFailedToLoad += HandleRewardBasedVideoFailedToLoad;
            // is opened.
            rewardBasedVideo.OnAdOpening += HandleRewardBasedVideoOpened;
            // has started playing.
            rewardBasedVideo.OnAdStarted += HandleRewardBasedVideoStarted;
            // has rewarded the user.
            rewardBasedVideo.OnAdRewarded += HandleRewardBasedVideoRewarded;
            // is closed.
            rewardBasedVideo.OnAdClosed += HandleRewardBasedVideoClosed;
            // is leaving the application.
            rewardBasedVideo.OnAdLeavingApplication += HandleRewardBasedVideoLeftApplication;
        }

        #region RewardBasedVideo callback handlers

        void HandleRewardBasedVideoLoaded (object sender, EventArgs args) {
            //      MonoBehaviour.print ("HandleRewardBasedVideoLoaded event received");
            //      Debug.Log ("読み込み");
        }

        void HandleRewardBasedVideoFailedToLoad (object sender, AdFailedToLoadEventArgs args) {
            //      MonoBehaviour.print (
            //          "HandleRewardBasedVideoFailedToLoad event received with message: " + args.Message);
            //      Debug.Log ("読み込み失敗");
            WaitingVideoManager.Instance.Failed ();  // 読み込み失敗ページを出す
        }

        void HandleRewardBasedVideoOpened (object sender, EventArgs args) {
            //      MonoBehaviour.print ("HandleRewardBasedVideoOpened event received");
            //      Debug.Log ("オープン");
        }

        void HandleRewardBasedVideoStarted (object sender, EventArgs args) {
            //      MonoBehaviour.print ("HandleRewardBasedVideoStarted event received");
            //      Debug.Log ("ビデオ開始");
        }

        public void HandleRewardBasedVideoClosed (object sender, EventArgs args) {
            //      MonoBehaviour.print ("HandleRewardBasedVideoClosed event received");
            //      Debug.Log ("ビデオ閉じた");
            WaitingVideoManager.Instance.Hide ();  // 読み込みページを閉じる
        }

        void HandleRewardBasedVideoRewarded (object sender, Reward args) {
            string type = args.Type;
            double amount = args.Amount;
            //      MonoBehaviour.print (
            //          "HandleRewardBasedVideoRewarded event received for " + amount.ToString () + " " + type);
            //      Debug.Log ("リワード成功");
            // リワード成功
            RewardManager.Instance.Reward ();
        }

        void HandleRewardBasedVideoLeftApplication (object sender, EventArgs args) {
            //      MonoBehaviour.print ("HandleRewardBasedVideoLeftApplication event received");
        }
        #endregion
    }
}
