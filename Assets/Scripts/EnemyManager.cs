﻿using System.Collections;
using UnityEngine;

namespace Chankou {

    /* 全ての敵アニマルを管理するクラス */

    public class EnemyManager : SingletonMonoBehaviour<EnemyManager>, IInitializable {

        // 全ての敵アニマルを格納する配列のプロパティ
        [SerializeField] Enemy[] enemyArray;
        public Enemy[] EnemyArray {
            private set { enemyArray = value; }
            get { return enemyArray; }
        }

        /* 
         生成した敵アニマルのIDを格納する配列
         （同じ種類のアニマルが生成されるなどの偏りをなくすために使用する）
        */
        public int[] AdjustArray { private set; get; }


        // 初期化
        public void Init () {
            EnemyArray = new Enemy[Constant.MAX_DISPLAYED_ENEMY];
            for (int y = Constant.LOWEST_ROW_INDEX; y >= 0; y -= Constant.ANIMAL_NUM) {
                Forward ();
                for (int x = 0; x < Constant.ANIMAL_NUM; x++) {
                    int id = DataManager.Instance.EnemyArray[x + y];

                    /*
                     セーブデータの途中からプレイする時に判定する
                     56ターン目以降は敵アニマルの後ろに透明なアニマルが存在している
                    */
                    int forwardId = 0;
                    for (int i = x + y + Constant.ANIMAL_NUM; i < Constant.MAX_DISPLAYED_ENEMY; i += Constant.ANIMAL_NUM) {
                        forwardId += DataManager.Instance.EnemyArray[i];
                    }
                    if (id != 0 || forwardId > 0) {
                        EnemyArray[x] = PoolManager.Instance.GetEnemy ();
                        EnemyArray[x].Active (id, PositionManager.Instance.EnemyPos[x]);
                    }
                }
            }
            AdjustArray = DataManager.Instance.AdjustArray;
        }

        // 敵アニマルを1行生成する
        public void MakeEnemyLine (int turn) {
            // 全ての敵アニマルを進ませる
            Forward ();
            for (int i = 0; i < GameManager.Instance.animalNum; i++) {
                // プールから取り出す
                EnemyArray[i] = PoolManager.Instance.GetEnemy ();
                int ID;
                if (GameManager.Instance.animalNum == 4) {
                    ID = GetRandomID1 (turn, i);
                } else if (GameManager.Instance.animalNum == 5) {
                    ID = GetRandomID2 (turn, i);
                } else {
                    ID = GetRandomID3 (turn, i);
                }
                if (1 <= ID && ID <= 6) {
                    // 偏りを調整するため
                    AdjustArray[ID - 1]++;
                }
                EnemyArray[i].Active (ID, PositionManager.Instance.EnemyPos[i]);
            }
        }

        // ランダムなIDを返す（難易度1）
        int GetRandomID1 (int turn, int index) {
            // 56ターン目以降はアニマルが出ない
            if (turn > Constant.FINISH_POINT) { return 0; }

            int random = Random.Range (0, 10);
            if (random == 0) {
                // 10%で隣のアニマル
                if (index >= 1) {
                    return EnemyArray[index - 1].AnimalID;
                } else {
                    // 偏りを調整する
                    return GetAdjustID ();
                }
            } else if (random == 1) {
                // 10%で前のアニマル
                if (EnemyArray[index + Constant.ANIMAL_NUM]) {
                    return EnemyArray[index + Constant.ANIMAL_NUM].AnimalID;
                }
            }
            // 90%でランダム
            return Random.Range (1, GameManager.Instance.animalNum + 1);
        }

        // ランダムなIDを返す（難易度2）
        int GetRandomID2 (int turn, int index) {
            // 56ターン目以降はアニマルが出ない
            if (turn > Constant.FINISH_POINT) { return 0; }

            int random = Random.Range (0, 10);
            if (random <= 1) {
                // 20%で隣のアニマル
                if (index >= 1) {
                    return EnemyArray[index - 1].AnimalID;
                } else {
                    // 偏りを調整する
                    return GetAdjustID ();
                }
            } else if (random == 2) {
                // 10%で前のアニマル
                if (EnemyArray[index + Constant.ANIMAL_NUM]) {
                    return EnemyArray[index + Constant.ANIMAL_NUM].AnimalID;
                }
            }
            // 70%でランダム
            return Random.Range (1, GameManager.Instance.animalNum + 1);
        }

        // ランダムなIDを返す（難易度3）
        int GetRandomID3 (int turn, int index) {
            // 56ターン目以降はアニマルが出ない
            if (turn > Constant.FINISH_POINT) { return 0; }

            int random = Random.Range (0, 10);
            if (random <= 2) {
                // 30%で隣のアニマル
                if (index >= 1) {
                    return EnemyArray[index - 1].AnimalID;
                } else {
                    // 偏りを調整する
                    return GetAdjustID ();
                }
            } else if (random <= 4) {
                // 20%で前のアニマル
                if (EnemyArray[index + Constant.ANIMAL_NUM]) {
                    return EnemyArray[index + Constant.ANIMAL_NUM].AnimalID;
                }
            }
            // 50%でランダム
            return Random.Range (1, GameManager.Instance.animalNum + 1);
        }

        // 偏りを調整する（頻度が最も少ないアニマルのIDを返す）
        int GetAdjustID () {
            int minIndex = 0;
            int number = 999;  // 頻度が最も少ないアニマルの数を代入する。
            for (int i = 0; i < GameManager.Instance.animalNum; i++) {
                if (AdjustArray[i] < number) {
                    number = AdjustArray[i];
                    minIndex = i;
                }
            }
            return minIndex + 1;
        }

        // 攻撃される
        public void Attack () {
            StartCoroutine (AttackCoroutine ());
        }

        // 攻撃するコルーチン
        IEnumerator AttackCoroutine () {
            bool[] isAttack = new bool[GameManager.Instance.animalNum];
            while (true) {
                bool isDefeat = false;
                for (int i = 0; i < GameManager.Instance.animalNum; i++) {
                    int index = GetFirstEnemyIndex (i);
                    if (index < Constant.MAX_DISPLAYED_ENEMY && isAttack[i] == false) {
                        if (IsSame (PlayerManager.Instance.PlayerArray[i].AnimalID, EnemyArray[index])) {
                            PlayerManager.Instance.AttackAction (index, i);
                            ComboManager.Instance.Combo++;
                            Defeat (index);
                            ConnectDefeat (PlayerManager.Instance.PlayerArray[i].AnimalID, index);
                            ScoreManager.Instance.FinishConnect ();  // つながる敵終了
                            isAttack[i] = true;
                            isDefeat = true;
                            yield return new WaitForSeconds (0.5f);
                            continue;
                        }
                    }
                }
                if (isDefeat == false) {
                    break;
                }
            }
            // ターン終了
            GameManager.Instance.EndTurn ();
        }

        // 隣の敵のインデックスのリストを返す
        void ConnectDefeat (int ID, int index) {
            // 上
            if (index >= Constant.HIGHEST_ROW_INDEX + Constant.ANIMAL_NUM) {
                int next = index - Constant.ANIMAL_NUM;
                if (EnemyArray[next] != null && IsSame (ID, EnemyArray[next])) {
                    Defeat (next);
                    ConnectDefeat (ID, next);
                }
            }
            // 下
            if (index < Constant.LOWEST_ROW_INDEX) {
                int next = index + Constant.ANIMAL_NUM;
                if (EnemyArray[next] != null && IsSame (ID, EnemyArray[next])) {
                    Defeat (next);
                    ConnectDefeat (ID, next);
                }
            }
            // 右
            if (index % Constant.ANIMAL_NUM != Constant.ANIMAL_NUM - 1) {
                int next = index + 1;
                if (EnemyArray[next] != null && IsSame (ID, EnemyArray[next])) {
                    Defeat (next);
                    ConnectDefeat (ID, next);
                }
            }
            // 左
            if (index % Constant.ANIMAL_NUM != 0) {
                int next = index - 1;
                if (EnemyArray[next] != null && IsSame (ID, EnemyArray[next])) {
                    Defeat (next);
                    ConnectDefeat (ID, next);
                }
            }
        }

        // 敵を倒す
        void Defeat (int index) {
            // 敵がやられる
            EnemyArray[index].DeActive ();
            // 敵アニマルをプールに戻す
            PoolManager.Instance.SetEnemy (EnemyArray[index]);
            // 参照を外す
            EnemyArray[index] = null;
            // つながる敵アニマルを加算
            ScoreManager.Instance.EnemyNumber++;
            // 倒した数アニマルを増やす
            NumberManager.Instance.Num++;
            // defeatSEを鳴らす
            AudioManager.Instance.Play (AudioManager.eAudioSource.DefeatSE);
        }

        // 画面内の4列の敵を倒す (コンティニュー)
        public void ContinueDefeat () {
            for (int i = Constant.ANIMAL_NUM; i < 30; i++) {
                if (EnemyArray[i] != null) {
                    EnemyArray[i].DeActive ();
                    if (EnemyArray[i].AnimalID != 0) {
                        NumberManager.Instance.Num++;
                    }
                    PoolManager.Instance.SetEnemy (EnemyArray[i]);
                    EnemyArray[i] = null;
                }
            }
            AudioManager.Instance.Play (AudioManager.eAudioSource.DefeatSE);
            // ターン終了
            GameManager.Instance.EndTurn ();
        }

        // スキルを使う
        public void SkillDefeat (int ID) {
            ComboManager.Instance.Combo++;
            for (int i = Constant.ANIMAL_NUM; i < EnemyArray.Length; i++) {
                if (EnemyArray[i] != null) {
                    if (EnemyArray[i].AnimalID == ID) {
                        EnemyArray[i].DeActive ();
                        PoolManager.Instance.SetEnemy (EnemyArray[i]);
                        EnemyArray[i] = null;
                        ScoreManager.Instance.EnemyNumber++;
                        NumberManager.Instance.Num++;
                    }
                }
            }
            AudioManager.Instance.Play (AudioManager.eAudioSource.DefeatSE);  // defeatSEを鳴らす
            ScoreManager.Instance.FinishConnect ();  // つながる敵終了
            ScoreManager.Instance.Turn ();  // 毎ターン呼ぶ
            ComboManager.Instance.Turn ();  // コンボを初期化
            EnemyManager.Instance.Back ();  // 敵が戻る処理
            GameManager.Instance.isAction = false;  // タッチの反応を有効にする
        }

        // その列の先頭の敵のインデックスを返す
        public int GetFirstEnemyIndex (int xPosIndex) {
            for (int i = Constant.LOWEST_ROW_INDEX + xPosIndex; i >= Constant.ANIMAL_NUM; i -= Constant.ANIMAL_NUM) {
                if (EnemyArray[i] != null) {
                    return i;
                }
            }
            return Constant.MAX_DISPLAYED_ENEMY;  // ( = return 54;)
        }

        // その敵アニマルと味方アニマルが同じ種類かどうか
        bool IsSame (int ID, Enemy enemy) {
            if (ID == enemy.AnimalID) {
                return true;
            }
            return false;
        }

        // 全ての敵アニマルが進む
        public void Forward () {
            for (int i = EnemyArray.Length - 1; i >= 0; i--) {
                if (EnemyArray[i] != null && i < Constant.LOWEST_ROW_INDEX) {
                    int nextIndex = i + Constant.ANIMAL_NUM;
                    EnemyArray[i].Move (nextIndex);
                    EnemyArray[nextIndex] = EnemyArray[i];
                    EnemyArray[i] = null;
                }
            }
        }

        // 敵アニマルが戻る（自分の後ろのアニマルが倒された時）
        public void Back () {
            for (int i = Constant.HIGHEST_ROW_INDEX + Constant.ANIMAL_NUM; i < EnemyArray.Length; i++) {
                if (EnemyArray[i] != null) {
                    int nextIndex = GetBackIndex (i);
                    if (nextIndex < i) {
                        EnemyArray[i].Move (nextIndex);
                        EnemyArray[nextIndex] = EnemyArray[i];
                        EnemyArray[i] = null;
                    }
                }
            }
        }

        // 戻る位置のインデックスを返す
        int GetBackIndex (int index) {
            int _index = index;
            int backNumber = 0;
            while (_index >= Constant.HIGHEST_ROW_INDEX + Constant.ANIMAL_NUM) {
                _index -= Constant.ANIMAL_NUM;
                if (EnemyArray[_index] == null) {
                    backNumber++;
                }
            }
            return index - (Constant.ANIMAL_NUM * backNumber);
        }

        // そのアニマルが画面内に存在するかどうか
        public bool IsExists (int id) {
            for (int i = Constant.ANIMAL_NUM; i < EnemyArray.Length; i++) {
                if (EnemyArray[i] != null) {
                    if (EnemyArray[i].AnimalID == id) {
                        return true;
                    }
                }
            }
            return false;
        }

        // ゲームオーバーかどうか
        public bool IsGameOver () {
            for (int i = Constant.LOWEST_ROW_INDEX; i < EnemyArray.Length; i++) {
                if (EnemyArray[i] != null) {
                    return true;
                }
            }
            return false;
        }
    }
}
