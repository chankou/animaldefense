﻿using UnityEngine;
using UnityEngine.UI;  // 追加
using DG.Tweening;  // 追加

namespace Chankou {

    /* スキルを管理するクラス */

    public class SkillManager : SingletonMonoBehaviour<SkillManager>, IInitializable {

        const int MAX_SKILL_NUMBER = 3;  // スキルを使用できる回数
        const int REWARD_SKILL_NUMBER = 3;  // 動画リワード広告を視聴することで、さらにスキルを使用できる回数

        [SerializeField] RectTransform trans;
        [SerializeField] Text skillText;
        [SerializeField] Text titleText;
        [SerializeField] Text bodyText;
        [SerializeField] Button skillButton;
        [SerializeField] Button[] buttons;  // 動物たちのボタン
        [SerializeField] ChooseAnimalPanel chooseAnimalPanel;

        // スキルを使用した回数
        [SerializeField] Text skillNumberText;
        int skillNumber;
        public int SkillNumber {
            set {
                skillNumber = value;
                skillButton.interactable = true;
                if (skillNumber < MAX_SKILL_NUMBER) {
                    skillNumberText.text = (MAX_SKILL_NUMBER - skillNumber).ToString ();
                } else if (SkillNumber >= MAX_SKILL_NUMBER + REWARD_SKILL_NUMBER) {
                    skillButton.interactable = false;
                    skillNumberText.text = "";
                } else {
                    skillNumberText.text = "";
                }
            }
            get { return skillNumber; }
        }

        void Start () {
            // ローカライズ
            skillText.text = TextManager.Get (TextManager.KEY.SKILL);
            titleText.text = TextManager.Get (TextManager.KEY.SKILL);
            bodyText.text = TextManager.Get (TextManager.KEY.SKILL_BODY);
        }

        // 初期化
        public void Init () {
            trans.localPosition = new Vector3 (0f, -1400f, 0f);
            gameObject.SetActive (false);
            SkillNumber = DataManager.Instance.SkillNumber;
        }

        // 動物のボタンが押された時
        public void OnButton (int id) {
            // ChooseAnimalPanel.Instance.Apper (id);
            chooseAnimalPanel.Apper (id);
        }

        // ボタンを有効にするかどうか
        void IsInteractable () {
            for (int i = 0; i < buttons.Length; i++) {
                if (EnemyManager.Instance.IsExists (i + 1)) {
                    buttons[i].interactable = true;
                } else {
                    buttons[i].interactable = false;
                }
            }
        }

        // パネルを出す
        public void Apper () {
            // タッチの反応を無効にする
            GameManager.Instance.isAction = true;
            // スキルは3回までしか使えない
            if (SkillNumber >= MAX_SKILL_NUMBER) {
                // 動画を見るか確認
                ContinueManager.Instance.ApperRewardSkill ();
                return;
            }
            IsInteractable ();  // ボタンを有効化
            trans.localPosition = new Vector3 (0f, -1400f, 0f);
            gameObject.SetActive (true);

            // y座標-6400fの位置に0.3f秒で移動する
            trans.DOLocalMoveY (-640f, 0.3f).SetEase (Ease.OutBack);
        }

        // スキルを閉じるボタンが押されたとき
        public void CloseButton () {
            // タッチの反応を有効にする
            GameManager.Instance.isAction = false;
            Hide ();
        }

        // パネルを隠す
        public void Hide () {
            // y座標-1400fの位置に0.3f秒で移動する
            trans.DOLocalMoveY (-1400f, 0.3f).SetEase (Ease.InBack)
                .OnComplete (() => {
                    gameObject.SetActive (false);
                });
            chooseAnimalPanel.Hide ();
        }

        // スキルを使用できる状態にする (動画リワード広告を見た時)
        public void SkillAvailable () {
            // スキルを一度だけ使えるようにする
            SkillManager.Instance.SkillNumber = MAX_SKILL_NUMBER - 1;
        }

        // スキルを使用できない状態にする （動画リワード広告を3回以上見た時）
        public void SkillUnavailable () {
            // 3回動画を見たら、もうスキルは使えない
            SkillManager.Instance.SkillNumber = MAX_SKILL_NUMBER + REWARD_SKILL_NUMBER;
        }

        public int GetRewardSkillNumber () {
            return REWARD_SKILL_NUMBER;
        }
    }
}
