﻿using UnityEngine;

namespace Chankou {

    /* 全ての味方アニマルを管理するクラス */

    public class PlayerManager : SingletonMonoBehaviour<PlayerManager>, IInitializable {

        [SerializeField] GameObject playerPrefab;
        public bool isMove;  // 味方アニマルが移動したかどうか

        // 全ての味方アニマルを格納する配列のプロパティ
        public Player[] PlayerArray {
            private set;
            get;
        }

        void Start () {
            PlayerArray = new Player[Constant.ANIMAL_NUM];

            // 生成する
            Vector3 pos = new Vector3 (0f, 0f, 0f);
            for (int i = 0; i < PlayerArray.Length; i++) {
                GameObject obj = (GameObject)Instantiate (
                    playerPrefab,
                    pos,
                    Quaternion.identity
                );
                PlayerArray[i] = obj.GetComponent<Player> ();
            }
        }

        // 初期化
        public void Init () {
            isMove = false;
            for (int i = 0; i < PlayerArray.Length; i++) {
                if (i < GameManager.Instance.animalNum) {
                    PlayerArray[i].Active (DataManager.Instance.PlayerArray[i], i);
                } else {
                    PlayerArray[i].gameObject.SetActive (false);
                }
            }
        }

        // アニマルの位置を入れ替える
        public void ChangePos (int before, int after) {
            isMove = true;
            Player player = PlayerArray[after];
            PlayerArray[after] = PlayerArray[before];
            PlayerArray[before] = player;
            player.Move (before);
        }

        // アニマルの種類（列数）を追加する
        public void AddAnimal (int ID, int index) {
            PlayerArray[index].Active (ID, index);
        }

        // 攻撃アニメをさせる
        public void AttackAction (int enemyIndex, int playerIndex) {
            PlayerArray[playerIndex].AttackAction (enemyIndex);
        }

        // 攻撃の後の戻る動作
        public void AfterAttack () {
            for (int i = 0; i < GameManager.Instance.animalNum; i++) {
                PlayerArray[i].Back ();
            }
        }
    }
}
