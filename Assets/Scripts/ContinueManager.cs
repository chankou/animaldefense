﻿using UnityEngine;
using UnityEngine.UI;  // 追加
using DG.Tweening;  // 追加

namespace Chankou {

    /* 
    * ゲームオーバーの時にコンティニューするためか、
    * スキル使用回数を増やすために、
    * 動画リワード広告の視聴を促すクラス
    */

    public class ContinueManager : SingletonMonoBehaviour<ContinueManager>, IInitializable {

        public bool isContinue = false;
        int mode = 0;  // 0:コンティニュー , 1:スキル
        int rewardNumber = 0;  // スキルリワード動画を見た回数

        [SerializeField] RectTransform trans;
        [SerializeField] Text titleText;
        [SerializeField] Text bodyText;
        [SerializeField] Text yesText;
        [SerializeField] Text noText;

        void Start () {
            // ローカライズ
            yesText.text = TextManager.Get (TextManager.KEY.CONTINUE_YES);
            noText.text = TextManager.Get (TextManager.KEY.CONTINUE_NO);
        }

        // 初期化
        public void Init () {
            trans.localPosition = new Vector3 (0f, -1000f, 0f);
            gameObject.SetActive (false);
            rewardNumber = 0;
            isContinue = DataManager.Instance.IsContinue;
        }

        // yesボタンが押された時
        public void YesButton () {
            RewardManager.Instance.ShowRewardVideo (mode);
            Hide ();  // パネルを隠す
        }

        // noボタンが押された時
        public void NoButton () {
            if (mode == 0) {
                // コンティニューモードのとき
                NoContinue ();
            } else {
                // タッチの反応を有効にする
                GameManager.Instance.isAction = false;
            }
            Hide ();  // パネルを隠す
        }

        // スキルリワード
        public void SkillReward () {
            rewardNumber++;
            // スキルを一度だけ使えるようにする
            SkillManager.Instance.SkillAvailable ();
            SkillManager.Instance.Apper ();

            // 3回動画を見たら、もうスキルは使えない
            if (rewardNumber >= SkillManager.Instance.GetRewardSkillNumber ()) {
                SkillManager.Instance.SkillUnavailable ();
            }
        }

        // コンティニューする場合
        public void Continue () {
            if (isContinue) {
                return;
            }
            isContinue = true;
            // タッチの反応を有効にする
            GameManager.Instance.isAction = false;
            // 敵を一掃する
            EnemyManager.Instance.ContinueDefeat ();
        }

        // コンティニューしない場合
        public void NoContinue () {
            isContinue = true;
            // ゲームオーバー
            GameManager.Instance.GameOver ();
        }

        // 「動画を見てコンティニューしますか」パネルを出す
        public void ApperContinue () {
            // コンティニューモードにする
            mode = 0;
            // ローカライズ
            titleText.text = TextManager.Get (TextManager.KEY.CONTINUE_TITLE);
            bodyText.text = TextManager.Get (TextManager.KEY.CONTINUE_BODY);

            // showSEを鳴らす
            AudioManager.Instance.Play (AudioManager.eAudioSource.ShowSE);

            trans.localPosition = new Vector3 (0f, -1000f, 0f);
            gameObject.SetActive (true);

            // y座標0fの位置に0.4f秒で移動する
            trans.DOLocalMoveY (0f, 0.4f).SetEase (Ease.OutBack);
        }

        // 「動画を見てスキル使用回数を増やしますか」パネルを出す
        public void ApperRewardSkill () {
            // リワードスキルモードにする
            mode = 1;
            // ローカライズ
            titleText.text = TextManager.Get (TextManager.KEY.SKILL);
            bodyText.text = TextManager.Get (TextManager.KEY.REWARDSKILL);

            // showSEを鳴らす
            AudioManager.Instance.Play (AudioManager.eAudioSource.ShowSE);

            trans.localPosition = new Vector3 (0f, -1000f, 0f);
            gameObject.SetActive (true);

            // y座標0fの位置に0.4f秒で移動する
            trans.DOLocalMoveY (0f, 0.4f).SetEase (Ease.OutBack);
        }

        // パネルを隠す
        public void Hide () {
            // y座標-1000fの位置に0.4f秒で移動する
            trans.DOLocalMoveY (-1000f, 0.4f).SetEase (Ease.InBack)
                .OnComplete (() => { gameObject.SetActive (false); });
        }
    }
}
