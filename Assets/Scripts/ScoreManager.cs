﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;  // 追加
using DG.Tweening;  // 追加

namespace Chankou {

    /* スコアを管理するクラス */

    public class ScoreManager : SingletonMonoBehaviour<ScoreManager>, IInitializable {

        //  スコアが何点加算されたかを表すテキストの位置の参照
        Transform addScoreTrans;

        // スコアのプロパティ
        [SerializeField] Text scoreLabel;
        [SerializeField] Text scoreText;
        int score;
        public int Score {
            private set {
                score = value;
                scoreText.text = score.ToString ();
                AddScore = 0;
            }
            get { return score; }
        }

        // スコアに何点加算されるかを表すテキストのプロパティ
        [SerializeField] Text addText;
        int addScore;
        public int AddScore {
            private set {
                addScore = value;
                addText.text = "+ " + (addScore).ToString ();
                TextAnimation ();
                if (addScore == 0) {
                    addScoreTrans.localScale = new Vector3 (0f, 0f, 0f);
                }
            }
            get { return addScore; }
        }

        public int EnemyNumber {
            set;
            get;
        }

        void Start () {
            // ローカライズ
            scoreLabel.text = TextManager.Get (TextManager.KEY.SCORE);
            addScoreTrans = addText.gameObject.transform;
            addScoreTrans.localScale = new Vector3 (0f, 0f, 0f);
        }

        // 初期化
        public void Init () {
            Score = DataManager.Instance.Score;
            AddScore = 0;
            EnemyNumber = 0;
        }

        // addScoreを表示するアニメーション
        void TextAnimation () {
            if (addScore == 0) {
                return;
            }
            addScoreTrans.localScale = new Vector3 (0f, 0f, 0f);
            addScoreTrans.DOScale (new Vector3 (1f, 1f, 1f), 0.2f).SetEase (Ease.OutBack);
        }

        // 毎ターン呼ばれる
        public void Turn () {
            StartCoroutine (UpScore ());
        }

        // スコアを更新する
        IEnumerator UpScore () {
            int combo = ComboManager.Instance.Combo;
            if (combo >= 2) {
                addText.text = addText.text + " × " + combo.ToString ();
            }

            // 0.5秒待つ
            yield return new WaitForSeconds (0.5f);

            Score += AddScore * combo;
            AddScore = 0;
        }

        // 最後のスコア加算される (リザルト画面へ移った時)
        public void UpScoreForResult () {
            Score += AddScore * ComboManager.Instance.Combo;
            AddScore = 0;
        }

        // 敵アニマルの同種類の繋がりが終了したとき
        public void FinishConnect () {
            AddScore += Sum (EnemyNumber);
            EnemyNumber = 0;
        }

        // 1~numberまでの総和を返す
        int Sum (int number) {
            int sum = 0;
            for (int i = 1; i <= number; i++) {
                sum += i;
            }
            return sum;
        }
    }
}
