﻿using UnityEngine;
using GoogleMobileAds.Api;  // 追加

namespace Chankou {

    /* Google AdMob広告のクラス */

    public class AdMobManager : MonoBehaviour {

#if UNITY_ANDROID
        readonly string Android_Banner = "ca-app-pub-5022505207706955/6961214827";
        readonly string Android_Interstitial = "ca-app-pub-5022505207706955/3868147623";
#endif
#if UNITY_IOS
        readonly string ios_Banner = "ca-app-pub-5022505207706955/4657210024";
        readonly string ios_Interstitial = "ca-app-pub-5022505207706955/6133943226";
#endif

        BannerView bannerView;

        InterstitialAd interstitial;
        AdRequest request;

        bool is_close_interstitial;

        // Use this for initialization
        void Awake () {
            // 起動時にインタースティシャル広告をロードしておく
            //RequestInterstitial ();
            // バナー広告を表示
            RequestBanner ();
        }

        public void RequestBanner () {
#if UNITY_ANDROID
            string adUnitId = Android_Banner;
#elif UNITY_IOS
            string adUnitId = ios_Banner;
#else
            string adUnitId = "unexpected_platform";
#endif

            // Create a 320x50 banner at the top of the screen.
            bannerView = new BannerView (adUnitId, AdSize.Banner, AdPosition.Bottom);
            // Create an empty ad request.
            AdRequest request = new AdRequest.Builder ().Build ();
            // Load the banner with the request.
            bannerView.LoadAd (request);
        }

        public void RequestInterstitial () {
#if UNITY_ANDROID
            string adUnitId = Android_Interstitial;
#elif UNITY_IOS
            string adUnitId = ios_Interstitial;
#else
            string adUnitId = "unexpected_platform";
#endif

            if (is_close_interstitial == true) {
                interstitial.Destroy ();
            }

            // Initialize an InterstitialAd.
            interstitial = new InterstitialAd (adUnitId);
            // Create an empty ad request.
            request = new AdRequest.Builder ().Build ();
            // Load the interstitial with the request.
            interstitial.LoadAd (request);

            interstitial.OnAdClosed += HandleAdClosed;
            interstitial.OnAdFailedToLoad += HandleAdReLoad;

            is_close_interstitial = false;
        }

        // インタースティシャル広告を閉じた時に走る
        void HandleAdClosed (object sender, System.EventArgs e) {
            is_close_interstitial = true;
            //      Debug.Log ("インタースティシャルを閉じた");
            RequestInterstitial ();
        }
        // 広告のロードに失敗したときに走る
        void HandleAdReLoad (object sender, System.EventArgs e) {
            is_close_interstitial = true;

            //      StartCoroutine(_waitConnect());
        }

        //  // 次のロードまで30秒待つ
        //  IEnumerator _waitConnect()
        //  {
        //      while (true)
        //      {
        //          yield return new WaitForSeconds(30.0f);
        //
        //          // ネットに接続できるときだけリロード
        //          if (Application.internetReachability != NetworkReachability.NotReachable)
        //          {
        //              RequestInterstitial();
        //              break;
        //          }
        //      }
        //  }

        public void ShowInterstitial () {
            if (interstitial.IsLoaded ()) {
                interstitial.Show ();
            }
        }

        // バナーを表示
        public void ShowBanner () {
            bannerView.Show ();
        }

        // バナーを隠す
        public void HideBanner () {
            bannerView.Hide ();
        }
    }
}
