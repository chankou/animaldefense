﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;  // 追加
using GoogleMobileAds.Api;  // 追加

namespace Chankou {

    /* 動画リワード広告のクラス */

    public class RewardManager : SingletonMonoBehaviour<RewardManager>, IInitializable {

        public bool isReward = false;  // リワード成功かどうか
        public int mode = 0;  // 0:コンティニュー , 1:スキル
        [SerializeField] GameObject continueButton;  // コンティニューボタン
        [SerializeField] Text text;

        void Start () {
            // ローカライズ
            text.text = TextManager.Get (TextManager.KEY.CONTINUE_TITLE);
        }

        public void Init () {
            continueButton.SetActive (false);
        }

        // 動画を見せる
        public void ShowRewardVideo (int _mode) {

            // コンティニューするか、スキル使用回数を増やすかのモードをセットする
            mode = _mode;

            isReward = false;

            // 動画読み込み画面を出す
            WaitingVideoManager.Instance.Appear ();

            // リワード広告を再生する
#if UNITY_ANDROID
            // android はunity ads
            if (UnityAdsManager.Instance.ShowAd ()) {
                // 動画が再生されるはずだから
                // リワード成功
                Reward ();
                WaitingVideoManager.Instance.Hide ();  // 読み込みページを閉じる
            } else {
                WaitingVideoManager.Instance.Failed ();  // 読み込み失敗ページを出す
            }
            return;
#endif

            StartCoroutine (WaitCoroutine ());
        }

        // 動画の読み込みを待つ
        IEnumerator WaitCoroutine () {
            bool isActive = false;
            int callNumber = 0;
            AdRewardManager.Instance.RequestRewardBasedVideo ();
            RewardBasedVideoAd rewardBasedVideo = RewardBasedVideoAd.Instance;

            while (!isActive) {

                // 1秒待つ
                yield return new WaitForSeconds (1f);

                callNumber++;
                // 動画の読み込みが完了していたら動画を見せる
                if (rewardBasedVideo.IsLoaded ()) {
                    rewardBasedVideo.Show ();
                    isActive = true;
                    break;
                } else {
                    // 9秒待っても読み込めなかったら、失敗
                    if (callNumber > 9) {
                        // 読み込み失敗ページを出す
                        WaitingVideoManager.Instance.Failed ();
                        isActive = true;
                        break;
                    }
                }
            }
        }

        // リワード
        public void Reward () {
            isReward = true;
            if (mode == 0) {
                // コンティニューボタンを出す
                continueButton.SetActive (true);
            } else {
                // スキルを使えるようにする
                ContinueManager.Instance.SkillReward ();
            }
        }

        // コンティニューボタンが押されたとき
        public void OnContinueButton () {
            // ボタンを非表示にする
            continueButton.SetActive (false);
            // コンティニューする
            ContinueManager.Instance.Continue ();
        }
    }
}
