﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Advertisements;  // unity Ads

namespace Chankou {

    public class UnityAdsManager : SingletonMonoBehaviour<UnityAdsManager> {

        public bool ShowAd () {
            if (Advertisement.IsReady ("rewardedVideo")) {
                ShowOptions options = new ShowOptions { resultCallback = HandleShowResult };
                Advertisement.Show ("rewardedVideo", options);
                return true;
            }
            return false;
        }

        void HandleShowResult (ShowResult _result) {
            switch (_result) {
                case ShowResult.Finished:
                    Debug.Log ("視聴完了");
                    break;
                default:
                    Debug.Log ("動画失敗");
                    break;
            }
        }
    }
}
