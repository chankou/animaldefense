﻿using UnityEngine;
using UnityEngine.UI;  // 追加
using DG.Tweening;  // 追加

namespace Chankou {

    /* アニマルの種類が追加された時に現れるパネルのクラス */

    public class AddAnimalPanel : MonoBehaviour {

        RectTransform myTrans;
        [SerializeField] Image addAnimalImage;
        [SerializeField] Text appearText;

        void Start () {
            appearText.text = TextManager.Get (TextManager.KEY.APPEAR);
            myTrans = gameObject.GetComponent<RectTransform> ();
            myTrans.localPosition = new Vector3 (700f, 0f, 0f);
            gameObject.SetActive (false);

        }

        // パネルが現れるアニメ
        public void Appear (int ID) {
            gameObject.SetActive (true);
            GameManager.Instance.isAction = true;
            addAnimalImage.sprite = SpriteManager.Instance.AnimalSprite[ID - 1];  // 画像を変える
            myTrans.localPosition = new Vector3 (700f, 0f, 0f);
            Sequence sequence = DOTween.Sequence ();
            sequence.Append (myTrans.DOLocalMoveX (0, 0.4f).SetEase (Ease.OutBack));
            sequence.Append (myTrans.DOLocalMoveX (0, 2.0f));
            sequence.Append (myTrans.DOLocalMoveX (-700, 0.4f).SetEase (Ease.InBack).OnComplete (
                () => {
                    gameObject.SetActive (false);
                    GameManager.Instance.isAction = false;
                }
            ));
        }
    }
}
