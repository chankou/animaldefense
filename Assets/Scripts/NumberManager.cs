﻿using UnityEngine;
using UnityEngine.UI;  // 追加

namespace Chankou {

    /* 倒した敵アニマルの数を管理するクラス */

    public class NumberManager : SingletonMonoBehaviour<NumberManager>, IInitializable {

        // 倒した敵アニマルの数
        [SerializeField] Text numLabel;
        [SerializeField] Text numText;
        int num;
        public int Num {
            set {
                num = value;
                numText.text = num.ToString ();
                // ゲーム終了
                if (num == Constant.ENEMY_NUM) {
                    // つながる敵終了
                    ScoreManager.Instance.FinishConnect ();

                    ContinueManager.Instance.isContinue = true;
                    GameManager.Instance.GameOver ();
                }
            }
            get { return num; }
        }

        void Start () {
            // ローカライズ
            numLabel.text = TextManager.Get (TextManager.KEY.NUMBER);
        }

        // 初期化
        public void Init () {
            Num = DataManager.Instance.Number;
        }
    }
}
