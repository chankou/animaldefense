﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;  // 追加

namespace Chankou {

    /* 動画リワード広告を読み込む時の待機画面のクラス */

    public class WaitingVideoManager : SingletonMonoBehaviour<WaitingVideoManager> {

        [SerializeField] Text text;
        [SerializeField] GameObject closeButton;

        void Start () {
            text.text = TextManager.Get (TextManager.KEY.WAIT);
            Init ();
        }

        // 初期化
        public void Init () {
            gameObject.SetActive (false);
            closeButton.SetActive (false);
        }

        // 動画読み込み中の画面を表示する
        public void Appear () {
            text.text = TextManager.Get (TextManager.KEY.WAIT);
            AudioManager.Instance.Stop (AudioManager.eAudioSource.GameBGM);

            // タッチの反応を無効にする
            GameManager.Instance.isAction = true;

            gameObject.SetActive (true);
            StartCoroutine (ForcingCoroutine ());
        }

        // 失敗ページ
        public void Failed () {
            text.text = TextManager.Get (TextManager.KEY.FAILVIDEO);
            closeButton.SetActive (true);
        }

        // 閉じるボタンが押されたとき
        public void CloseButton () {
            Hide ();
        }

        // 動画読み込み中の画面を閉じる
        public void Hide () {
            // コンティニュー動画が見れなかったとき
            if (RewardManager.Instance.mode == 0 && RewardManager.Instance.isReward == false) {
                // ゲームを終了させる
                ContinueManager.Instance.NoContinue ();
            } else {
                if (RewardManager.Instance.mode == 1 && RewardManager.Instance.isReward == false) {
                    // タッチの反応を有効にする
                    GameManager.Instance.isAction = false;
                }
                // gameBGMを再生する
                AudioManager.Instance.Play (AudioManager.eAudioSource.GameBGM);
            }
            Init ();
        }

        // 10秒待っても読み込まれない時は強制終了させる
        IEnumerator ForcingCoroutine () {
            yield return new WaitForSeconds (10f);
            Failed ();
        }
    }
}
