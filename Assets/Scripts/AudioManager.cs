﻿using UnityEngine;

namespace Chankou {

    /* サウンドを管理するクラス */

    public class AudioManager : SingletonMonoBehaviour<AudioManager> {

        [SerializeField] AudioSource titleBGM;
        [SerializeField] AudioSource gameBGM;
        [SerializeField] AudioSource defeatSE;
        [SerializeField] AudioSource showSE;
        [SerializeField] AudioSource resultJingle;

        public enum eAudioSource {
            TitleBGM,
            GameBGM,
            DefeatSE,
            ShowSE,
            ResultJingle
        }

        AudioSource GetAudioSource (eAudioSource audioSource) {
            switch (audioSource) {
                case eAudioSource.TitleBGM:
                    return titleBGM;
                case eAudioSource.GameBGM:
                    return gameBGM;
                case eAudioSource.DefeatSE:
                    return defeatSE;
                case eAudioSource.ShowSE:
                    return showSE;
                case eAudioSource.ResultJingle:
                    return resultJingle;
                default:
                    return null;
            }
        }

        public void Play (eAudioSource audioSource) {
            AudioSource audio = GetAudioSource (audioSource);
            audio.Play ();
        }

        public void Stop (eAudioSource audioSource) {
            AudioSource audio = GetAudioSource (audioSource);
            audio.Stop ();
        }
    }
}
