﻿using UnityEngine;
using UnityEngine.UI;  // 追加
using DG.Tweening;  // 追加

namespace Chankou {

    /* コンボを管理するクラス */

    public class ComboManager : SingletonMonoBehaviour<ComboManager>, IInitializable {

        Transform comboLabelTrans;
        Transform comboTextTrans;

        // コンボのプロパティ
        [SerializeField] Text comboLabel; // "こんぼ"
        [SerializeField] Text comboText;
        int combo;
        public int Combo {
            set {
                combo = value;
                comboText.text = combo.ToString ();
                TextAnimation ();
            }
            get { return combo; }
        }

        void Start () {
            // テキストをローカライズ
            comboLabel.text = TextManager.Get (TextManager.KEY.COMBO);
            comboLabelTrans = comboLabel.gameObject.transform;
            comboTextTrans = comboText.gameObject.transform;
        }

        // 初期化
        public void Init () {
            Combo = 0;
            // サイズを0にする
            Vector3 size0 = new Vector3 (0f, 0f, 0f);
            comboLabelTrans.localScale = size0;
            comboTextTrans.localScale = size0;
        }

        // 毎ターン呼ばれる処理
        public void Turn () {
            Init ();
        }

        // コンボを表示するアニメーション
        void TextAnimation () {
            if (Combo <= 1) {
                return;
            }
            Vector3 size0 = new Vector3 (0f, 0f, 0f);
            comboLabelTrans.localScale = size0;
            comboTextTrans.localScale = size0;

            Vector3 size1 = new Vector3 (1f, 1f, 1f);
            float changeTime = 0.2f;
            comboLabelTrans.DOScale (size1, changeTime).SetEase (Ease.OutBack);
            comboTextTrans.DOScale (size1, changeTime).SetEase (Ease.OutBack);
        }
    }
}
