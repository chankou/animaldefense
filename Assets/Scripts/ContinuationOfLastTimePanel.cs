﻿using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

namespace Chankou {

    // 前回のセーブデータの続きからプレイする時に表示するパネルのスクリプト

    public class ContinuationOfLastTimePanel : MonoBehaviour {

        RectTransform myTrans;
        [SerializeField] Text continuationText;

        // Use this for initialization
        void Start () {
            myTrans = gameObject.GetComponent<RectTransform> ();
            myTrans.localPosition = new Vector3 (0f, 0f, 0f);
            continuationText.text = TextManager.Get (TextManager.KEY.CONTINUATION_OF_LAST_TIME);
            gameObject.SetActive (false);
        }

        // パネルが現れて消えるアニメ
        public void Appear () {
            gameObject.SetActive (true);
            GameManager.Instance.isAction = true;

            myTrans.localPosition = new Vector3 (0f, 0f, 0f);
            Disappear ();
        }

        void Disappear () {
            // 時間差で呼び出す
            float waitTime = 2f;
            myTrans.DOLocalMoveY (2000f, 0.4f).SetEase (Ease.InBack).SetDelay (waitTime).OnComplete (
                () => {
                    gameObject.SetActive (false);
                    GameManager.Instance.isAction = false;
                }
            );
        }
    }
}
