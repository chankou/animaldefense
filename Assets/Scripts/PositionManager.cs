﻿using UnityEngine;

namespace Chankou {

    /* アニマルの位置の座標を持つクラス */

    public class PositionManager : SingletonMonoBehaviour<PositionManager> {

        public float[] boundary;

        // 敵アニマルの位置のプロパティ
        Vector3[] enemyPos;
        public Vector3[] EnemyPos {
            private set { enemyPos = value; }
            get { return enemyPos; }
        }

        // 味方アニマルの位置のプロパティ
        Vector3[] playerPos;
        public Vector3[] PlayerPos {
            private set { playerPos = value; }
            get { return playerPos; }
        }

        void Start () {
            SetEnemyPos ();  // 位置をセットする
            SetPlayerPos ();  // 位置をセットする
            boundary = new float[] {
            -1.28f, -0.64f, 0f, 0.64f, 1.28f};
        }

        // enemyPosをセットする
        void SetEnemyPos () {
            Vector3[] array = new Vector3[Constant.MAX_DISPLAYED_ENEMY];
            float[] xPos = { -1.6f, -0.96f, -0.32f, 0.32f, 0.96f, 1.6f };
            float[] yPos = { 3.82f, 3.18f, 2.54f, 1.9f, 1.26f, 0.62f, -0.02f, -0.66f, -1.3f };
            for (int y = 0; y < yPos.Length; y++) {
                for (int x = 0; x < xPos.Length; x++) {
                    array[(y * xPos.Length) + x] = new Vector3 (xPos[x], yPos[y], 1f);
                }
            }
            EnemyPos = array;
        }

        // playerPosをセットする
        void SetPlayerPos () {
            Vector3[] array = new Vector3[Constant.ANIMAL_NUM];
            float[] xPos = { -1.6f, -0.96f, -0.32f, 0.32f, 0.96f, 1.6f };
            for (int x = 0; x < xPos.Length; x++) {
                array[x] = new Vector3 (xPos[x], Constant.PLAYER_POS_Y, 1f);
            }
            PlayerPos = array;
        }

        // 味方アニマルに位置インデックスを返す
        public int GetPlayerPosIndex (Vector3 pos) {
            if (pos.x > boundary[4] && GameManager.Instance.animalNum == Constant.ANIMAL_NUM) {
                return 5;
            } else if (pos.x > boundary[3] && GameManager.Instance.animalNum >= Constant.ANIMAL_NUM - 1) {
                return 4;
            } else if (pos.x > boundary[2]) {
                return 3;
            } else if (pos.x > boundary[1]) {
                return 2;
            } else if (pos.x > boundary[0]) {
                return 1;
            } else {
                return 0;
            }
        }
    }
}
