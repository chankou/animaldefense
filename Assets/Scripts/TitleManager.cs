﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;  // 追加
using DG.Tweening;  // 追加

namespace Chankou {

    /* タイトル画面を管理するクラス */

    public class TitleManager : SingletonMonoBehaviour<TitleManager> {

        Coroutine coroutine;  // インタースティシャル広告のコルーチン
        [SerializeField] AdMobManager admobManager;
        [SerializeField] Transform trans;
        [SerializeField] Text titleText;
        [SerializeField] Text playText;
        [SerializeField] Text highScoreLabel;
        [SerializeField] Text worldRankLabel;
        [SerializeField] Text howText;
        [SerializeField] TitleAnimal[] titleAnimal;
        [SerializeField] WorldRanking worldRanking;

        // ハイスコアのプロパティ
        [SerializeField] Text highScoreText;
        int highScore;
        public int HighScore {
            private set {
                highScore = value;
                highScoreText.text = highScore.ToString ();
            }
            get { return highScore; }
        }

        // 世界ランクのプロパティ
        [SerializeField] Text worldRankText;
        int worldRank;
        public int WorldRank {
            set {
                worldRank = value;
                if (worldRank == 0) {
                    worldRankText.text = "-";
                } else {
                    worldRankText.text = worldRank.ToString ();
                }
            }
            get { return worldRank; }
        }


        void Start () {
            // ローカライズ
            titleText.text = TextManager.Get (TextManager.KEY.TITLE);
            playText.text = TextManager.Get (TextManager.KEY.PLAY);
            highScoreLabel.text = TextManager.Get (TextManager.KEY.HIGHSCORE);
            worldRankLabel.text = TextManager.Get (TextManager.KEY.WORLDRANK);
            howText.text = TextManager.Get (TextManager.KEY.HOW_TITLE);
            Init ();

            // 50%の確率でインタースティシャル広告を表示させる
            if (Random.Range (0, 2) == 0) {
                coroutine = StartCoroutine (InterstitialCoroutine ());
            }
        }

        // 初期化
        public void Init () {
            gameObject.SetActive (true);
            admobManager.ShowBanner ();
            AudioManager.Instance.Play (AudioManager.eAudioSource.TitleBGM);
            PlayAnimalAnime ();
            trans.DOLocalMove (new Vector3 (0f, 0f, 0f), 0.2f);  // 初期位置
            HighScore = DataManager.Instance.HighScore;

            // 会員登録が済ませてあるならばランキングを取得する
            if (DataManager.Instance.UserName != "") {
                worldRanking.PushHighScore (DataManager.Instance.UserName, HighScore);  // ハイスコアランキング
            }
        }

        // 動物にアニメをさせる
        public void PlayAnimalAnime () {
            for (int i = 0; i < titleAnimal.Length; i++) {
                titleAnimal[i].PlayAnime ();
            }
        }

        // 動物にアニメをやめさせる
        public void StopAnimalAnime () {
            for (int i = 0; i < titleAnimal.Length; i++) {
                titleAnimal[i].StopAnime ();
            }
        }

        // タイトル画面を隠す
        public void Play () {
            admobManager.HideBanner ();

            // インタースティシャルコルーチンを止める
            if (coroutine != null) {
                StopCoroutine (coroutine);
            }
            GameManager.Instance.Init ();
            AudioManager.Instance.Stop (AudioManager.eAudioSource.TitleBGM);
            StopAnimalAnime ();
            trans.DOLocalMoveX (-1000f, 0.2f).OnComplete (() => {
                gameObject.SetActive (false);
            });  // 初期位置

            // 初めてのプレイならば遊び方を見せる
            if (DataManager.Instance.HighScore == 0) {
                HowManager.Instance.Show ();
            }
        }

        // 4秒後にインタースティシャルを出す
        IEnumerator InterstitialCoroutine () {
            yield return new WaitForSeconds (0.1f);

            // インタースティシャルを読み込む
            admobManager.RequestInterstitial ();

            yield return new WaitForSeconds (4f);

            admobManager.ShowInterstitial ();
        }
    }
}
