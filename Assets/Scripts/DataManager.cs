﻿namespace Chankou {

    /* セーブデータを管理するクラス */

    public class DataManager : SingletonMonoBehaviour<DataManager> {

        void Awake () {
            DontDestroyOnLoad (gameObject);
        }

        readonly string FILE_NAME = "myFile.txt";
        readonly string USER_NAME = "username";
        readonly string HIGH_SCORE = "highscore";
        readonly string SCORE = "score";
        readonly string NUMBER = "number";
        readonly string TURN = "turn";
        readonly string SKILL_NUMBER = "skillnumber";
        readonly string PLAYER_ARRAY = "playerarray";
        readonly string ENEMY_ARRAY = "enemyarray";
        readonly string ADJUST_ARRAY = "adjustarray";
        readonly string IS_CONTINUE = "iscontinue";

        public string UserName {
            set { SaveUserName (value); }
            get { return LoadUserName (); }
        }

        public int HighScore {
            set { SaveHighScore (value); }
            get { return LoadHighScore (); }
        }

        // ゲームの途中をセーブ  ------------------------------
        public bool IsSave {
            set { }
            get { return Score > 0; }
        }
        public int Score {
            set { SaveScore (value); }
            get { return LoadScore (); }
        }
        public int Number {
            set { SaveNumber (value); }
            get { return LoadNumber (); }
        }
        public int Turn {
            set { SaveTurn (value); }
            get { return LoadTurn (); }
        }
        public int AnimalNumber {
            set { }
            get {
                if (Turn > Constant.SECOND_POINT) { return Constant.FIRST_ANIMAL_NUM + 2; } else if (Turn > Constant.FIRST_POINT) { return Constant.FIRST_ANIMAL_NUM + 1; }
                return Constant.FIRST_ANIMAL_NUM;
            }
        }
        public int SkillNumber {
            set { SaveSkillNumber (value); }
            get { return LoadSkillNunmber (); }
        }
        public int[] PlayerArray {
            set { SavePlayerArray (value); }
            get { return LoadPlayerArray (); }
        }
        public int[] EnemyArray {
            set { SaveEnemyArray (value); }
            get { return LoadEnemyArray (); }
        }
        public int[] AdjustArray {
            set { SaveAdjustArray (value); }
            get { return LoadAdjustArray (); }
        }
        public bool IsContinue {
            set { SaveIsContinue (value); }
            get { return LoadIsContinue (); }
        }
        //  ------------------------------------------------------------


        // 新規ユーザー名をセーブする
        void SaveUserName (string username) {
            using (ES2Writer writer = ES2Writer.Create (FILE_NAME)) {
                writer.Write (username, USER_NAME);
                writer.Save ();
            }
        }

        // ハイスコアをセーブする
        void SaveHighScore (int score) {
            using (ES2Writer writer = ES2Writer.Create (FILE_NAME)) {
                writer.Write (score, HIGH_SCORE);
                writer.Save ();
            }
        }

        // スコアをセーブする
        void SaveScore (int score) {
            using (ES2Writer writer = ES2Writer.Create (FILE_NAME)) {
                writer.Write (score, SCORE);
                writer.Save ();
            }
        }

        // 倒した数をセーブする
        void SaveNumber (int number) {
            using (ES2Writer writer = ES2Writer.Create (FILE_NAME)) {
                writer.Write (number, NUMBER);
                writer.Save ();
            }
        }

        // ターンをセーブする
        void SaveTurn (int turn) {
            using (ES2Writer writer = ES2Writer.Create (FILE_NAME)) {
                writer.Write (turn, TURN);
                writer.Save ();
            }
        }

        // スキル回数をセーブする
        void SaveSkillNumber (int skillNumber) {
            using (ES2Writer writer = ES2Writer.Create (FILE_NAME)) {
                writer.Write (skillNumber, SKILL_NUMBER);
                writer.Save ();
            }
        }

        // 仲間配列をセーブする
        void SavePlayerArray (int[] array) {
            using (ES2Writer writer = ES2Writer.Create (FILE_NAME)) {
                writer.Write (array, PLAYER_ARRAY);
                writer.Save ();
            }
        }

        // 敵配列をセーブする
        void SaveEnemyArray (int[] array) {
            using (ES2Writer writer = ES2Writer.Create (FILE_NAME)) {
                writer.Write (array, ENEMY_ARRAY);
                writer.Save ();
            }
        }

        // 偏り調整配列をセーブする
        void SaveAdjustArray (int[] array) {
            using (ES2Writer writer = ES2Writer.Create (FILE_NAME)) {
                writer.Write (array, ADJUST_ARRAY);
                writer.Save ();
            }
        }

        // コンティニューをセーブする
        void SaveIsContinue (bool flag) {
            using (ES2Writer writer = ES2Writer.Create (FILE_NAME)) {
                writer.Write (flag, IS_CONTINUE);
                writer.Save ();
            }
        }





        // ユーザー名をロード
        string LoadUserName () {
            using (ES2Reader reader = ES2Reader.Create (FILE_NAME)) {
                if (reader.TagExists (USER_NAME)) {
                    return reader.Read<string> (USER_NAME);
                } else {
                    UserName = "";
                    return "";
                }
            }
        }

        // ハイスコアをロード
        int LoadHighScore () {
            using (ES2Reader reader = ES2Reader.Create (FILE_NAME)) {
                if (reader.TagExists (HIGH_SCORE)) {
                    return reader.Read<int> (HIGH_SCORE);
                } else {
                    HighScore = 0;
                    return 0;
                }
            }
        }

        // スコアをロード
        int LoadScore () {
            using (ES2Reader reader = ES2Reader.Create (FILE_NAME)) {
                if (reader.TagExists (SCORE)) {
                    return reader.Read<int> (SCORE);
                } else {
                    Score = 0;
                    return 0;
                }
            }
        }

        // 倒した数をロード
        int LoadNumber () {
            using (ES2Reader reader = ES2Reader.Create (FILE_NAME)) {
                if (reader.TagExists (NUMBER)) {
                    return reader.Read<int> (NUMBER);
                } else {
                    Number = 0;
                    return 0;
                }
            }
        }

        // ターンをロード
        int LoadTurn () {
            using (ES2Reader reader = ES2Reader.Create (FILE_NAME)) {
                if (reader.TagExists (TURN)) {
                    return reader.Read<int> (TURN);
                } else {
                    Turn = 0;
                    return 0;
                }
            }
        }

        // スキル回数をロード
        int LoadSkillNunmber () {
            using (ES2Reader reader = ES2Reader.Create (FILE_NAME)) {
                if (reader.TagExists (SKILL_NUMBER)) {
                    return reader.Read<int> (SKILL_NUMBER);
                } else {
                    SkillNumber = 0;
                    return 0;
                }
            }
        }

        // 仲間配列をロード
        int[] LoadPlayerArray () {
            using (ES2Reader reader = ES2Reader.Create (FILE_NAME)) {
                if (reader.TagExists (PLAYER_ARRAY)) {
                    return reader.ReadArray<int> (PLAYER_ARRAY);
                } else {
                    int[] array = new int[Constant.ANIMAL_NUM];
                    for (int i = 0; i < array.Length; i++) {
                        array[i] = i + 1;
                    }
                    PlayerArray = array;
                    return array;
                }
            }
        }

        // 敵配列をロード
        int[] LoadEnemyArray () {
            using (ES2Reader reader = ES2Reader.Create (FILE_NAME)) {
                if (reader.TagExists (ENEMY_ARRAY)) {
                    return reader.ReadArray<int> (ENEMY_ARRAY);
                } else {
                    int[] array = new int[Constant.MAX_DISPLAYED_ENEMY];
                    EnemyArray = array;
                    return array;
                }
            }
        }

        // 偏り調整配列をロード
        int[] LoadAdjustArray () {
            using (ES2Reader reader = ES2Reader.Create (FILE_NAME)) {
                if (reader.TagExists (ADJUST_ARRAY)) {
                    return reader.ReadArray<int> (ADJUST_ARRAY);
                } else {
                    int[] array = new int[Constant.ANIMAL_NUM];
                    array[4] = 20;  // カエルを20匹セットする
                    array[5] = 40;  // ゾウを40匹セットする
                    AdjustArray = array;
                    return array;
                }
            }
        }

        // コンティニューをロード
        bool LoadIsContinue () {
            using (ES2Reader reader = ES2Reader.Create (FILE_NAME)) {
                if (reader.TagExists (IS_CONTINUE)) {
                    return reader.Read<bool> (IS_CONTINUE);
                } else {
                    IsContinue = false;
                    return false;
                }
            }
        }
    }
}
