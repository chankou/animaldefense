﻿using UnityEngine;
using DG.Tweening;  // 追加

namespace Chankou {

    /* アニマルの親クラス */

    public class Animal : MonoBehaviour {

        [SerializeField] protected SpriteRenderer spriteRenderer;  // アニマルの画像
        protected Transform myTrans;  // アニマルの位置

        /* AnimalID = ( 1:クマ , 2:ネコ , 3:パンダ , 4:ヒヨコ , 5:カエル , 6:ゾウ ) */

        // アニマルIDのプロパティ
        protected int animalID;
        public int AnimalID {
            protected set {
                animalID = value;

                // animalIDが 1 ~ 6 の場合、対応する画像をセットする
                if (1 <= animalID && animalID <= 6) {
                    spriteRenderer.sprite = SpriteManager.Instance.AnimalSprite[animalID - 1];
                } else {
                    spriteRenderer.sprite = null;
                }
            }
            get { return animalID; }
        }

        protected virtual void Awake () {
            myTrans = this.transform;
        }

        // 受け取った位置へ移動する
        protected void Move (Vector3 pos) {
            float moveTime = 0.2f;
            myTrans.DOMove (pos, moveTime);
        }
    }
}
