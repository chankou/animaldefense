﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;  // 追加

namespace Chankou {

    /* 遊び方説明時の敵アニマルのクラス */

    public class EnemyForHowToPlay : MonoBehaviour {

        Image image;
        Sprite sprite;
        Coroutine coroutine;

        void Awake () {
            image = gameObject.GetComponent<Image> ();
            sprite = image.sprite;
        }

        // 初期化
        public void Init () {
            image.sprite = sprite;
            gameObject.SetActive (true);
        }

        // やられる
        public void DeActive () {
            coroutine = StartCoroutine (DeactiveAnime ());
        }

        // やられるアニメ
        IEnumerator DeactiveAnime () {
            image.sprite = SpriteManager.Instance.DestroySprite;

            // 0.5秒待つ
            yield return new WaitForSeconds (0.5f);

            gameObject.SetActive (false);
        }

        // アニメをストップする
        public void StopAnime () {
            if (coroutine != null) {
                StopCoroutine (coroutine);
            }
        }
    }
}
