﻿using UnityEngine;

namespace Chankou {

    /* 
 敵アニマルのオブジェクトプールのクラス
 オブジェクトの生成コストを減らすため、
 予め画面内に表示し得る敵アニマルを生成しておき、
 オブジェクトの表示（生成）、非表示（破棄）を切り替える
*/

    public class PoolManager : SingletonMonoBehaviour<PoolManager>, IInitializable {

        [SerializeField] GameObject enemyPrefab;

        GameObject[] objectPool;
        Enemy[] enemyPool;
        int setIndex;
        int getIndex;

        void Start () {
            SetObjectPool ();
        }

        // 初期化
        public void Init () {
            setIndex = 0;
            getIndex = 0;
            SetEnemyPool ();  // 新しくプールをセットする
            InitAllEnemy ();  // すべての敵アニマルを初期化
        }

        // すべての敵アニマルを初期化する
        public void InitAllEnemy () {
            for (int i = 0; i < enemyPool.Length; i++) {
                enemyPool[i].Init ();
            }
        }

        // プールをセットする
        void SetEnemyPool () {
            enemyPool = new Enemy[Constant.MAX_DISPLAYED_ENEMY];
            for (int i = 0; i < enemyPool.Length; i++) {
                enemyPool[i] = objectPool[i].GetComponent<Enemy> ();
            }
        }

        // オブジェクトプールをセットする
        void SetObjectPool () {
            objectPool = new GameObject[Constant.MAX_DISPLAYED_ENEMY];
            for (int i = 0; i < objectPool.Length; i++) {
                objectPool[i] = MakeObject (enemyPrefab);
            }
        }

        // オブジェクトを生成する
        GameObject MakeObject (GameObject prefab) {
            GameObject obj = (GameObject)Instantiate (
                prefab,
                new Vector3 (0f, 0f, 0f),
                Quaternion.identity
            );
            return obj;
        }

        // 敵アニマルをプールに戻す
        public void SetEnemy (Enemy enemy) {
            enemyPool[setIndex] = enemy;
            if (setIndex == Constant.MAX_DISPLAYED_ENEMY - 1) {
                setIndex = 0;
            } else {
                setIndex++;
            }
        }

        // プールから敵アニマルを渡す
        public Enemy GetEnemy () {
            int beforeIndex = getIndex;
            if (getIndex == Constant.MAX_DISPLAYED_ENEMY - 1) {
                getIndex = 0;
            } else {
                getIndex++;
            }
            return enemyPool[beforeIndex];
        }
    }
}
