﻿using System.Collections.Generic;
using UnityEngine;
using System;  // 追加
using System.IO;  // 追加

namespace Chankou {

    /* ゲームで表示される全ての文字列の情報を持つクラス */

    public class TextManager : MonoBehaviour {

        static Dictionary<string, string> sDictionary = new Dictionary<string, string> ();

        // 検索キー
        public enum KEY {
            TITLE,
            PLAY,
            HIGHSCORE,
            WORLDRANK,
            SCORE,
            COMBO,
            NUMBER,
            APPEAR,
            QUIT,
            RELOAD,
            SOUND,
            MENU,
            CLOSE,
            SKILL,
            SKILL_BODY,
            SKILL_ACTIVE,
            CHOOSE,
            CHOOSE_YES,
            CHOOSE_NO,
            CONTINUATION_OF_LAST_TIME,
            CONTINUE_TITLE,
            CONTINUE_BODY,
            CONTINUE_YES,
            CONTINUE_NO,
            REWARDSKILL,
            WAIT,
            FAILVIDEO,
            CLEAR,
            FAILED,
            NEWRECORD,
            BACK,
            HOW_TITLE,
            HOW1,
            HOW2,
            HOW3,
            HOW4,
            HOW5,
            HOW6,
            ENEMY,
            FRIEND
        }

        // 使用言語
        public enum LANGUAGE {
            JAPANESE,
            ENGLISH,
            CHINESESIMPLEFIED,
            CHINESETRADITIONAL
        }

        // 文字列初期化
        public static void Init (LANGUAGE lang) {
            // リソースファイルパス決定
            string filePath;
            if (lang == LANGUAGE.JAPANESE) {
                filePath = "LocalText/japanese";
            } else if (lang == LANGUAGE.ENGLISH) {
                filePath = "LocalText/english";
            } else if (lang == LANGUAGE.CHINESESIMPLEFIED) {
                filePath = "LocalText/chinese_simplified";
            } else if (lang == LANGUAGE.CHINESETRADITIONAL) {
                filePath = "LocalText/chinese_traditional";
            } else {
                throw new Exception ("TextManager Init failed.");
            }

            // dictionary初期化
            sDictionary.Clear ();
            TextAsset csv = Resources.Load<TextAsset> (filePath);
            StringReader reader = new StringReader (csv.text);
            while (reader.Peek () > -1) {
                string[] values = reader.ReadLine ().Split (',');
                sDictionary.Add (values[0], values[1].Replace ("\\n", "\n").Replace ("<", ","));
            }
        }

        // 文字列取得
        // <param name = "key">文字列取得キー</param>
        // <returns>キーに該当する文字列</returns>
        public static string Get (KEY key) {
            return Get (Enum.GetName (typeof (KEY), key));
        }

        // 文字列取得
        // <param name = "key">文字列取得キー</param>
        // <returns>キーに該当する文字列</returns>
        public static string Get (string key) {
            return sDictionary[key];
        }
    }
}
