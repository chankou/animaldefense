﻿using System.Collections.Generic;
using UnityEngine;
using NCMB;  // 追加

namespace Chankou {

    /* スコアの世界ランキングのクラス */

    public class WorldRanking : MonoBehaviour {

        // ハイスコア送信
        public void PushHighScore (string username, int highScore) {
            // データストアの「HighScore」クラスから、nameをキーにして検索
            NCMBQuery<NCMBObject> query = new NCMBQuery<NCMBObject> ("HighScore");
            query.WhereEqualTo ("name", username);
            query.FindAsync ((List<NCMBObject> objList, NCMBException e) => {

                if (e != null) {
                    //検索失敗
                    TitleManager.Instance.WorldRank = 0;
                } else {
                    //検索成功
                    // ハイスコアが未登録だったら
                    if (objList.Count == 0) {
                        NCMBObject obj = new NCMBObject ("HighScore");
                        obj["name"] = username;
                        obj["score"] = 0;
                        obj.SaveAsync ();
                    }
                    // ハイスコアが登録済みだったら
                    else {
                        // ハイスコアを更新
                        if (System.Convert.ToInt32 (objList[0]["score"]) < highScore) {
                            objList[0]["score"] = highScore;
                            objList[0].SaveAsync ();
                        }
                    }
                    fetchRank ();
                }
            });
        }

        // 現プレイヤーのハイスコアを受けとってランクを取得 ---------------
        void fetchRank () {
            // データスコアの「HighScore」から検索
            NCMBQuery<NCMBObject> rankQuery = new NCMBQuery<NCMBObject> ("HighScore");
            rankQuery.WhereGreaterThan ("score", DataManager.Instance.HighScore);
            rankQuery.CountAsync ((int count, NCMBException e) => {

                if (e != null) {
                    //件数取得失敗
                    TitleManager.Instance.WorldRank = 0;
                } else {
                    //件数取得成功
                    TitleManager.Instance.WorldRank = count + 1;
                }
            });
        }
    }
}
