﻿using System.Collections;
using UnityEngine;

namespace Chankou {

    /* 敵アニマルのクラス */

    public class Enemy : Animal {

        protected override void Awake () {
            base.Awake ();
        }

        // 初期化
        public void Init () {
            spriteRenderer.sortingOrder = 0;
            gameObject.SetActive (false);
        }

        // 現れる
        public void Active (int id, Vector3 pos) {
            AnimalID = id;
            myTrans.position = pos;
            gameObject.SetActive (true);
        }

        // やられる
        public void DeActive () {
            StartCoroutine (DeactiveAnime ());
        }

        // やられるアニメ
        IEnumerator DeactiveAnime () {
            if (AnimalID != 0) {
                spriteRenderer.sortingOrder = -1;
                spriteRenderer.sprite = SpriteManager.Instance.DestroySprite;
            }

            // 0.5秒待つ
            yield return new WaitForSeconds (0.5f);

            Init ();
        }

        // 移動する
        public void Move (int index) {
            base.Move (PositionManager.Instance.EnemyPos[index]);
        }
    }
}
