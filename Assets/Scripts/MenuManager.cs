﻿using UnityEngine;
using UnityEngine.UI;  // 追加
using DG.Tweening;  // 追加

namespace Chankou {

    /* メニューを管理するクラス */

    public class MenuManager : SingletonMonoBehaviour<MenuManager> {

        [SerializeField] RectTransform trans;
        [SerializeField] Sprite[] soundSprite;  // インデックス = 0:音あり、 1:音なし
        [SerializeField] Text backText;
        [SerializeField] Text reloadText;
        [SerializeField] Text soundText;
        [SerializeField] Text closeText;
        [SerializeField] Text menuText;

        // サウンドの有無のプロパティ
        [SerializeField] Image soundImage;
        bool isSound;
        public bool IsSound {
            private set {
                isSound = value;
                if (isSound) {
                    soundImage.sprite = soundSprite[0];
                    AudioListener.volume = 1f;  // 音を出す
                } else {
                    soundImage.sprite = soundSprite[1];
                    AudioListener.volume = 0f;  // 音を消す
                }
            }
            get { return isSound; }
        }

        void Start () {
            // ローカライズ
            backText.text = TextManager.Get (TextManager.KEY.QUIT);
            reloadText.text = TextManager.Get (TextManager.KEY.RELOAD);
            soundText.text = TextManager.Get (TextManager.KEY.SOUND);
            closeText.text = TextManager.Get (TextManager.KEY.CLOSE);
            menuText.text = TextManager.Get (TextManager.KEY.MENU);
            IsSound = true;
        }

        // 初期化
        public void Init () {
            trans.localPosition = new Vector3 (0f, -1000f, 0f);
        }

        // サウンドボタンが押された時
        public void SoundButton () {
            if (IsSound) {
                IsSound = false;
            } else {
                IsSound = true;
            }
        }

        // リロードボタンが押された時
        public void ReloadButton () {
            // セーブデータ消去
            GameManager.Instance.ResetSave ();

            AudioManager.Instance.Stop (AudioManager.eAudioSource.GameBGM);
            GameManager.Instance.Init ();
        }

        // バックボタンが押された時
        public void BackButton () {
            GameManager.Instance.SaveGame ();
            AudioManager.Instance.Stop (AudioManager.eAudioSource.GameBGM);
            TitleManager.Instance.Init ();
        }

        // メニューを表示する
        public void Appear () {
            trans.localPosition = new Vector3 (0f, -1000f, 0f);
            gameObject.SetActive (true);
            trans.DOLocalMoveY (-640f, 0.1f);
        }

        // メニューを非表示にする
        public void Hide () {
            trans.DOLocalMoveY (-1000f, 0.1f)
                .OnComplete (() => { gameObject.SetActive (false); });
        }
    }
}
