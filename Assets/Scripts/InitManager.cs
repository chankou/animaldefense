﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;  // 追加
using UnityEngine.SceneManagement;  // 追加
using NCMB;  // 追加

namespace Chankou {

    /* アプリ起動時の画面の処理をするクラス */

    public class InitManager : MonoBehaviour {

        int roopCount = 0;  // 新規ユーザー登録でループした回数
        [SerializeField] Text text;

        void Start () {
            // ローカライズの言語設定
            if (Application.systemLanguage == SystemLanguage.Japanese) {
                TextManager.Init (TextManager.LANGUAGE.JAPANESE);
            }
            // フォントが中国語に対応していないため保留
            //else if (Application.systemLanguage == SystemLanguage.ChineseSimplified) { 
            //    TextManager.Init (TextManager.LANGUAGE.CHINESESIMPLEFIED);
            //} 
            //else if (Application.systemLanguage == SystemLanguage.ChineseTraditional) {  
            //    TextManager.Init (TextManager.LANGUAGE.CHINESETRADITIONAL);
            //}
            else {
                TextManager.Init (TextManager.LANGUAGE.ENGLISH);
            }
            // ローカライズ
            text.text = TextManager.Get (TextManager.KEY.WAIT);
            // 最初にEasySaveのファイルを生成する
            using (ES2Writer writer = ES2Writer.Create ("myFile.txt")) {
                writer.Save ();
            }
            // デバッグ用
#if UNITY_EDITOR
            GoGameScene ();
            return;
#endif
            // 新規登録の場合分け
            if (DataManager.Instance.UserName == "") {
                CountUser ();
                // 通信がうまく行かなくても、10秒後に強制移動する
                StartCoroutine (ForcingMove ());
            } else {
                // 登録が完了していたら次の画面に進む
                GoGameScene ();
            }
        }

        // ユーザー数を数える (ハイスコアを保存しているクラスのオブジェクト数から求める)
        void CountUser () {
            NCMBQuery<NCMBObject> userNumber = new NCMBQuery<NCMBObject> ("HighScore");
            userNumber.CountAsync ((int count, NCMBException e) => {
                if (e != null) {
                    //              Debug.Log ("ユーザ数取得に失敗" + e.ErrorMessage);
                    GoGameScene ();
                } else {
                    //              Debug.Log ("ユーザ数取得に成功");
                    // 登録した順番の数字をユーザー名にする
                    RegistrationUser ((count + 1).ToString ());
                }
            });
        }

        // 新規ユーザー登録する
        void RegistrationUser (string username) {
            // NCMBユーザーのインスタンス生成
            NCMBUser user = new NCMBUser ();
            username = "user" + username;
            // ユーザー名とパスワードの設定
            user.UserName = username;
            user.Password = "password";

            // 会員登録を行う
            user.SignUpAsync ((NCMBException e) => {
                if (e != null) {
                    //              Debug.Log ("新規登録に失敗" + e.ErrorMessage);
                    roopCount++;
                    if (roopCount > 10) {
                        GoGameScene ();
                    }
                    // もし失敗したら、ランダムな数字にする
                    RegistrationUser (((int)Random.Range (1000000, 9999999)).ToString ());
                } else {
                    //              Debug.Log ("新規登録に成功");
                    // 新規ハイスコア登録
                    RegistrationHighScore (username);
                }
            });
        }

        // 新規ハイスコア登録
        void RegistrationHighScore (string username) {
            NCMBObject obj = new NCMBObject ("HighScore");
            obj["name"] = username;
            obj["score"] = 0;
            obj.SaveAsync ((NCMBException e) => {
                if (e != null) {
                    //              Debug.Log ("新規ハイスコア登録に失敗" + e.ErrorMessage);
                    GoGameScene ();
                } else {
                    //              Debug.Log ("新規オールスター登録に成功");
                    // 初期設定が完全終了したから、ユーザー名をセーブする
                    DataManager.Instance.UserName = username;
                    GoGameScene ();
                }
            });
        }


        // 通信がうまく行かなくても、10秒後に強制移動する
        IEnumerator ForcingMove () {
            // 10秒待つ
            yield return new WaitForSeconds (10f);

            GoGameScene ();
        }

        // ゲームシーンへ遷移
        void GoGameScene () {
            SceneManager.LoadSceneAsync ("Scene_Game");
        }
    }
}
