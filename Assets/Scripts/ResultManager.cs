﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;  // 追加
using DG.Tweening;  // 追加

namespace Chankou {

    /* リザルト画面を管理するクラス */

    public class ResultManager : SingletonMonoBehaviour<ResultManager>, IInitializable {

        [SerializeField] AdMobManager admobManager;
        [SerializeField] RectTransform trans;
        [SerializeField] Text titleText;
        [SerializeField] Text scoreLabel;
        [SerializeField] Text scoreText;
        [SerializeField] Text numlabel;
        [SerializeField] Text numText;
        [SerializeField] Text newRecordText;
        [SerializeField] Text backText;
        [SerializeField] GameObject skillButton;
        [SerializeField] GameObject menuButton;
        [SerializeField] GameObject menu;

        void Start () {
            // ローカライズ
            scoreLabel.text = TextManager.Get (TextManager.KEY.SCORE);
            numlabel.text = TextManager.Get (TextManager.KEY.NUMBER);
            newRecordText.text = TextManager.Get (TextManager.KEY.NEWRECORD);
            backText.text = TextManager.Get (TextManager.KEY.BACK);
        }

        // 初期化
        public void Init () {
            trans.localPosition = new Vector3 (0f, -1400f, 0f);
            gameObject.SetActive (false);
            skillButton.SetActive (true);
            menuButton.SetActive (true);
            menu.SetActive (true);
        }

        // リザルト画面を表示する
        public void Appear () {
            gameObject.SetActive (true);
            AudioManager.Instance.Play (AudioManager.eAudioSource.ResultJingle);
            trans.localPosition = new Vector3 (0f, -1400f, 0f);
            if (NumberManager.Instance.Num == Constant.ENEMY_NUM) {
                titleText.text = TextManager.Get (TextManager.KEY.CLEAR);
            } else {
                titleText.text = TextManager.Get (TextManager.KEY.FAILED);
            }
            ScoreManager.Instance.UpScoreForResult ();  // 最後のスコア加算
            scoreText.text = ScoreManager.Instance.Score.ToString ();
            numText.text = NumberManager.Instance.Num.ToString ();
            trans.DOLocalMoveY (0f, 0.4f).SetEase (Ease.OutBack);
            skillButton.SetActive (false);
            menuButton.SetActive (false);
            menu.SetActive (false);

            int score = ScoreManager.Instance.Score;
            if (score > DataManager.Instance.HighScore) {
                // ハイスコアを更新する
                DataManager.Instance.HighScore = score;
                newRecordText.gameObject.SetActive (true);
            } else {
                newRecordText.gameObject.SetActive (false);
            }
            // インタースティシャル広告を出す
            StartCoroutine (InterstitialCoroutine ());
        }

        // 3秒後にインタースティシャル広告を出す
        IEnumerator InterstitialCoroutine () {
            admobManager.RequestInterstitial ();

            // 3秒待つ
            yield return new WaitForSeconds (3f);

            admobManager.ShowInterstitial ();
        }

        // バックボタン
        public void BackButton () {
            // タイトル画面へ戻る
            TitleManager.Instance.Init ();
        }

        // リザルト画面を非表示にする
        public void Hide () {
            gameObject.SetActive (false);
        }
    }
}
