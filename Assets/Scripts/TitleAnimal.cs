﻿using System.Collections;
using UnityEngine;

namespace Chankou {

    /* タイトル画面に表示されるアニマルのクラス */

    public class TitleAnimal : MonoBehaviour {

        [SerializeField] RectTransform trans;
        Coroutine coroutine;

        // アニメスタート
        public void PlayAnime () {
            coroutine = StartCoroutine (Animation ());
        }

        // アニメストップ
        public void StopAnime () {
            if (coroutine != null) {
                StopCoroutine (coroutine);
            }
        }

        // BGMにノリノリのアニメーション
        IEnumerator Animation () {
            float waitTime = 0.451f;
            float angle = 5f;
            while (true) {
                trans.rotation = Quaternion.Euler (0f, 0f, angle);
                yield return new WaitForSeconds (waitTime);
                trans.rotation = Quaternion.Euler (0f, 0f, -angle);
                yield return new WaitForSeconds (waitTime);
            }
        }
    }
}
