﻿using UnityEngine;

namespace Chankou {

    /* アニマルの画像の情報をもつクラス */

    public class SpriteManager : SingletonMonoBehaviour<SpriteManager> {

        // アニマルの画像のプロパティ
        [SerializeField] Sprite[] sprite;
        public Sprite[] AnimalSprite {
            private set { sprite = value; }
            get { return sprite; }
        }

        // 消えた時の画像のプロパティ
        [SerializeField] Sprite destroySprite;
        public Sprite DestroySprite {
            private set { destroySprite = value; }
            get { return destroySprite; }
        }
    }
}
