﻿using System.Collections;
using UnityEngine;

namespace Chankou {

    /* ゲーム全体を管理するクラス */

    public class GameManager : SingletonMonoBehaviour<GameManager> {

        public int turn;  // 現在のターン
        public int animalNum;  // アニマルの種類の数（列数）
        public bool isAction;  // 処理中かどうか（trueならば、タッチの反応を無効にする）
        bool isGameOver;  // ゲームオーバーかどうか

        [SerializeField] Transform cameraTrans;  // カメラの位置
        [SerializeField] AddAnimalPanel addAnimalPanel;  // アニマルの種類が追加された時に現れるパネル
        [SerializeField] ContinuationOfLastTimePanel continuationOfLastTimePanel;  // 前回のつづきから始めるときに現れる画面

        // 初期化
        public void Init () {
            turn = DataManager.Instance.Turn;
            animalNum = DataManager.Instance.AnimalNumber;
            isAction = false;
            isGameOver = false;

            // カメラの位置を設定する
            SetCameraPos (animalNum);

            AudioManager.Instance.Play (AudioManager.eAudioSource.GameBGM);

            // 初期化する
            PoolManager.Instance.Init ();
            EnemyManager.Instance.Init ();
            PlayerManager.Instance.Init ();
            ScoreManager.Instance.Init ();
            NumberManager.Instance.Init ();
            ComboManager.Instance.Init ();
            ContinueManager.Instance.Init ();
            ResultManager.Instance.Init ();
            MenuManager.Instance.Init ();
            SkillManager.Instance.Init ();
            RewardManager.Instance.Init ();

            if (!DataManager.Instance.IsSave) {
                FirstSetEnemy ();
            } else {
                continuationOfLastTimePanel.Appear ();
            }
        }

        // 最初に敵アニマルをセットする
        void FirstSetEnemy () {
            for (int i = 0; i < Constant.FIRST_ANIMAL_NUM; i++) {
                EnemyManager.Instance.MakeEnemyLine (turn);
            }
        }

        // 毎ターン呼ばれる
        public void EveryTurn () {
            isAction = true;
            EnemyManager.Instance.Attack ();
        }

        // ターン終了コールバック
        public void EndTurn () {
            if (isGameOver) {
                return;
            }
            StartCoroutine (EndAction ());
        }

        // 処理を制御する
        IEnumerator EndAction () {
            ScoreManager.Instance.Turn ();  // 毎ターン呼ぶ
            ComboManager.Instance.Turn ();  // コンボを初期化
            EnemyManager.Instance.Back ();  // 敵が戻る処理
            PlayerManager.Instance.AfterAttack ();  // 攻撃した仲間を戻す

            // 0.5秒待つ
            yield return new WaitForSeconds (0.5f);

            isAction = false;
            AddAnimal ();
            if (EnemyManager.Instance.IsGameOver ()) {
                GameOver ();
            } else {
                turn++;
                // 敵か進む
                EnemyManager.Instance.MakeEnemyLine (turn);
            }
        }

        // アニマルの種類を追加する
        void AddAnimal () {
            if (turn == Constant.FIRST_POINT) {
                animalNum = 5;  // アニマルの種類が4から5になる
                SetCameraPos (animalNum);
                addAnimalPanel.Appear (animalNum);  // 出現パネル
                PlayerManager.Instance.AddAnimal (animalNum, animalNum - 1);  // アニマルの種類を追加する
            } else if (turn == Constant.SECOND_POINT) {
                animalNum = 6;  // アニマルの種類が5から6になる
                SetCameraPos (animalNum);
                addAnimalPanel.Appear (animalNum);  // 出現パネル
                PlayerManager.Instance.AddAnimal (animalNum, animalNum - 1);  // アニマルの種類を追加する
            }
        }

        // アニマルの種類に応じてカメラの位置を変える
        void SetCameraPos (int animalNum) {
            switch (animalNum) {
                case 4:
                    cameraTrans.position = new Vector3 (-0.64f, 0f, -10f);
                    break;
                case 5:
                    cameraTrans.position = new Vector3 (-0.32f, 0f, -10f);
                    break;
                case 6:
                    cameraTrans.position = new Vector3 (-0, 0f, -10f);
                    break;
            }
        }

        // ゲームオーバー
        public void GameOver () {
            // タッチを反応させない
            isAction = true;

            if (ContinueManager.Instance.isContinue == false) {
                // コンティニューパネルを出す
                ContinueManager.Instance.ApperContinue ();
                return;
            }
            isGameOver = true;
            AudioManager.Instance.Stop (AudioManager.eAudioSource.GameBGM);

            // セーブデータ消去
            ResetSave ();

            // リザルト画面を表示する
            ResultManager.Instance.Appear ();
        }

        // 途中をセーブする
        public void SaveGame () {
            if (isAction) {
                return;  // 動作中ならばセーブさせない
            }
            if (ScoreManager.Instance.Score < 1) {
                return;  // スコアが0ならばセーブさせない
            }
            DataManager dm = DataManager.Instance;
            dm.Score = ScoreManager.Instance.Score;
            dm.Number = NumberManager.Instance.Num;
            dm.Turn = turn;
            dm.SkillNumber = SkillManager.Instance.SkillNumber;
            Player[] playerArray = PlayerManager.Instance.PlayerArray;
            int[] playerIndexArray = new int[playerArray.Length];
            for (int i = 0; i < playerIndexArray.Length; i++) {
                playerIndexArray[i] = playerArray[i].AnimalID;
            }
            dm.PlayerArray = playerIndexArray;
            Enemy[] enemyArray = EnemyManager.Instance.EnemyArray;
            int[] enemyIndexArray = new int[enemyArray.Length];
            for (int i = 0; i < enemyArray.Length; i++) {
                if (enemyArray[i] != null) {
                    enemyIndexArray[i] = enemyArray[i].AnimalID;
                }
            }
            dm.EnemyArray = enemyIndexArray;
            dm.AdjustArray = EnemyManager.Instance.AdjustArray;
            dm.IsContinue = ContinueManager.Instance.isContinue;
        }

        // セーブデータを解除
        public void ResetSave () {
            DataManager dm = DataManager.Instance;
            dm.Score = 0;
            dm.Number = 0;
            dm.Turn = 0;
            dm.SkillNumber = 0;
            int[] playerArray = new int[Constant.ANIMAL_NUM];
            for (int i = 0; i < playerArray.Length; i++) {
                playerArray[i] = i + 1;
            }
            dm.PlayerArray = playerArray;
            dm.EnemyArray = new int[Constant.MAX_DISPLAYED_ENEMY];
            int[] adjustArray = new int[Constant.ANIMAL_NUM];
            adjustArray[4] = 20;  // カエルを20匹セットする
            adjustArray[5] = 40;  // ゾウを40匹セットする
            dm.AdjustArray = adjustArray;
            dm.IsContinue = false;
        }

        // バックグラウンドでの処理
        void OnApplicationPause (bool pauseStatus) {
            if (pauseStatus) {
                SaveGame ();
            }
        }
    }
}
