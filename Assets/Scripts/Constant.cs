﻿namespace Chankou {

    public static class Constant {
        public const int ENEMY_NUM = 300;  // 敵の最大数
        public const int MAX_DISPLAYED_ENEMY = 54;  // 画面に表示する敵の最大数
        public const int FIRST_POINT = 16;  // アニマルの種類（列数）が増えるターン
        public const int SECOND_POINT = 36;  // アニマルの種類（列数）が増えるターン
        public const int FINISH_POINT = 56;  // 最後のターン（56ターンでちょうど300匹のアニマルがでる）
        public const int ANIMAL_NUM = 6;  // アニマルの種類の最大数（列数）
        public const int FIRST_ANIMAL_NUM = 4;  // アニマルの動物の種類の数（列数）
        public const int HIGHEST_ROW_INDEX = ANIMAL_NUM;  // 画面内に表示される敵の最上行のはじめのインデックス
        public const int LOWEST_ROW_INDEX = MAX_DISPLAYED_ENEMY - ANIMAL_NUM;  // 画面内に表示される敵の最下行のはじめのインデックス
        public const float PLAYER_POS_Y = -1.94f;  // プレイヤーのY座標
    }
}
