﻿using UnityEngine;
using UnityEngine.UI;  // 追加
using DG.Tweening;  // 追加

namespace Chankou {

    /* スキル使用時、アニマルを選んだ時のパネルを処理するクラス */

    public class ChooseAnimalPanel : MonoBehaviour {

        int ID;  // 選んだ動物のID

        [SerializeField] RectTransform trans;
        [SerializeField] RectTransform skillActivePanel;
        [SerializeField] Image image;
        [SerializeField] Text titleText;
        [SerializeField] Text yesText;
        [SerializeField] Text noText;
        [SerializeField] Text skillActiveText;

        void Start () {
            // ローカライズ
            titleText.text = TextManager.Get (TextManager.KEY.CHOOSE);
            yesText.text = TextManager.Get (TextManager.KEY.CHOOSE_YES);
            noText.text = TextManager.Get (TextManager.KEY.CHOOSE_NO);
            skillActiveText.text = TextManager.Get (TextManager.KEY.SKILL_ACTIVE);
            Init ();
        }

        // 初期化
        public void Init () {
            trans.localPosition = new Vector3 (0f, -1000f, 0f);
            skillActivePanel.localPosition = new Vector3 (-2000f, 0f, 0f);
            gameObject.SetActive (false);
        }

        // スキルアニメ
        void SkillAnimation () {
            skillActivePanel.gameObject.SetActive (true);
            skillActivePanel.localPosition = new Vector3 (-2000f, 0f, 0f);
            Sequence sequence = DOTween.Sequence ();
            sequence.Append (skillActivePanel.DOLocalMoveX (0f, 0.4f));
            sequence.Append (skillActivePanel.DOLocalMoveX (0f, 1f));
            sequence.Append (skillActivePanel.DOLocalMoveX (2000f, 0.4f).OnComplete (() => {
                // スキルが発動し、選んだ動物を全て倒す
                EnemyManager.Instance.SkillDefeat (ID);
                skillActivePanel.gameObject.SetActive (false);
            }));
        }

        // 選ぶ
        public void Choose () {
            SkillAnimation ();  // スキル発動
            SkillManager.Instance.SkillNumber++;  // スキル回数を増やす
            SkillManager.Instance.Hide ();
        }

        // パネルを出す
        public void Apper (int id) {
            ID = id;
            image.sprite = SpriteManager.Instance.AnimalSprite[id - 1];
            trans.localPosition = new Vector3 (0f, -1000f, 0f);
            gameObject.SetActive (true);

            // y座標-200fの位置に0.4f秒で移動する
            trans.DOLocalMoveY (-200f, 0.4f).SetEase (Ease.OutBack);
        }

        // パネルを隠す
        public void Hide () {
            // y座標-1000fの位置に0.4f秒で移動する
            trans.DOLocalMoveY (-1000f, 0.4f).SetEase (Ease.InBack)
                .OnComplete (() => { gameObject.SetActive (false); });
        }
    }
}
